clear all


edgelist_source = importdata("686.edges");
nodelist1 = unique(edgelist_source);

edgelist_target = importdata("698.edges");
nodelist2 = unique(edgelist_target);

[commonIds,common_node_source_idx,common_node_target_idx] = intersect(nodelist1,nodelist2);

