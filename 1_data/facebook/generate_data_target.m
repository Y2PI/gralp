clc
clear all
close all


%% Read edgelists (Target)
edgelist_target = importdata('698.edges');
nodelist_target = unique(edgelist_target);
% Generate a weight matrix from the edge-list

W_target = zeros(length(nodelist_target),length(nodelist_target));
for peredge = 1 : size(edgelist_target,1)
    edge_start = find(nodelist_target ==edgelist_target(peredge,1));
    edge_dest = find(nodelist_target ==edgelist_target(peredge,2));
    W_target(edge_start,edge_dest)= 1;
    W_target(edge_dest,edge_start) = 1;
end

D = diag(sum(W_target,1));
W_target_norm = D^(-1/2)*W_target*D^(-1/2);  
W_target_dis = 1 - W_target_norm; 
for i = 1 : size(W_target_dis)
    W_target_dis(i,i) = 0; 
end
% g = graph(W_target); 
% W_target_dis = distances(g);
% W_target_dis((W_target_dis == Inf)) = 10;


[target_data,s] = mdscale(W_target_dis,5);
s
save target_data target_data


figure; imagesc(target_data*target_data')
figure; imagesc(W_target)
figure; imagesc(W_target_norm)