clc
clear all
close all


%% Read edgelists (Target)
edgelist_target = importdata('698.edges');
nodelist_target = unique(edgelist_target);
% Generate a weight matrix from the edge-list

W_target = zeros(length(nodelist_target),length(nodelist_target));
for peredge = 1 : size(edgelist_target,1)
    edge_start = find(nodelist_target ==edgelist_target(peredge,1));
    edge_dest = find(nodelist_target ==edgelist_target(peredge,2));
    W_target(edge_start,edge_dest)= 1;
    W_target(edge_dest,edge_start) = 1;
end


%% Read all features for all nodes (target)
features_and_nodes_target = importdata("698.feat");
nodelist2 = features_and_nodes_target(:,1);
% Strangely node lists differ from .edges to .feat 
nodelist_target_for_feats = zeros(length(nodelist_target),1);
for pernode = 1 : length(nodelist_target)
    nodelist_target_for_feats(pernode) = find(nodelist2 == nodelist_target(pernode));
end
    
% Get all features
allfeatures_target = features_and_nodes_target(nodelist_target_for_feats,2:end);
% Gender label is at 27-28 columns. See .featnames file
gender_label_target = allfeatures_target(:,27:28); 

save W_target W_target
save gender_label_target gender_label_target

load prev_weigth_label.mat
isequal(W2,W_target)
isequal(gender_label_target',f_target)
assert(isequal(W2,W_target))
assert(isequal(gender_label_target',f_target))

