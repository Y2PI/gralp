clear all
clc
close all

%% Read edgelists (Source)
edgelist_source = importdata('686.edges');
nodelist_source = unique(edgelist_source);
% Generate a weight matrix from the edge-list
W_source = zeros(length(nodelist_source),length(nodelist_source));
for peredge = 1 : size(edgelist_source,1)
    edge_start = find(nodelist_source ==edgelist_source(peredge,1));
    edge_dest = find(nodelist_source ==edgelist_source(peredge,2));
    W_source(edge_start,edge_dest)= 1;
    W_source(edge_dest,edge_start) = 1;
end

%% Read all features for all nodes (source)
features_and_nodes_source = importdata("686.feat");
nodelist1 = features_and_nodes_source(:,1);
% Strangely node lists differ from .edges to .feat 
nodelist_source_for_feats = zeros(length(nodelist_source),1);
for pernode = 1 : length(nodelist_source)
    nodelist_source_for_feats(pernode) = find(nodelist1 == nodelist_source(pernode));
end
    
% Get all features
allfeatures_source = features_and_nodes_source(nodelist_source_for_feats,2:end);
% Gender label is at 42-43 columns. See .featnames file
gender_label_source = allfeatures_source(:,42:43); 



save W_source W_source
save gender_label_source gender_label_source

load prev_weigth_label.mat
isequal(W1,W_source)
isequal(gender_label_source',f_source)
assert(isequal(W1,W_source))
assert(isequal(gender_label_source',f_source))

