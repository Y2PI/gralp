clc
clear all
close all


%% Read edgelists (source)
edgelist_source = importdata('686.edges');
nodelist_source = unique(edgelist_source);
% Generate a weight matrix from the edge-list

W_source = zeros(length(nodelist_source),length(nodelist_source));
for peredge = 1 : size(edgelist_source,1)
    edge_start = find(nodelist_source ==edgelist_source(peredge,1));
    edge_dest = find(nodelist_source ==edgelist_source(peredge,2));
    W_source(edge_start,edge_dest)= 1;
    W_source(edge_dest,edge_start) = 1;
end

D = diag(sum(W_source,1));
W_source_norm = D^(-1/2)*W_source*D^(-1/2);  
W_source_dis = 1 - W_source_norm; 
for i = 1 : size(W_source_dis)
    W_source_dis(i,i) = 0; 
end

% g = graph(W_source); 
% W_source_dis = distances(g);

[source_data,s] = mdscale(W_source_dis,5);
s
save source_data source_data


figure; imagesc(source_data*source_data')
figure; imagesc(W_source)