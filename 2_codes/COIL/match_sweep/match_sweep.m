clear all
close all
clc

%% LOAD DATA
DataLoc = '../../../1_data/COIL';
load '../../../1_data/COIL/COIL_source.mat'
load '../../../1_data/COIL/COIL_target.mat'

s = [9,19,16,17,13,12,14,18,2,4];
t = [5,6,8,15,10,11,7,20,1,3];

data = [ COIL_source COIL_target];
for(i=1:10)
    source{i,1} = data{s(i)};
    target{i,1} = data{t(i)};
end
COIL_source = source;
COIL_target = target;

%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/COIL/match/NodeStruct.mat
load ../../../3_outputs/COIL/Dict_Source.mat
W_source = Dict;
load ../../../3_outputs/COIL/Dict_Target.mat
W_target = Dict;
numberofWavelets = 5;

addpath("../wda_functions")
paramlength = length(NodeStruct(1).targetknownnodes);

%% GENERATE GRAPHS AND LABELS
sigma = 0.2;
k = 5;

[W1,f_source,source_data] = CalculateWeightAndLabels_new(COIL_source,sigma,k);
numofNodeSource = length(f_source);
% Target
[W2,f_target,target_data] = CalculateWeightAndLabels_new(COIL_target,sigma,k);
numofNodeTarget = length(f_target);


%% Hyperparameters
mu =(10^(0)); 
gama_t =(10^(-1));
gama_s = (10^(-1));

%% Experiments
ntrial = length(NodeStruct);
errors_wda = zeros(paramlength,ntrial)
for itrial = 1 : ntrial
    for iparam = 1 : paramlength
        display(["itrial" itrial "iparam" iparam])
        drawnow
        %% Get parameter setting
        target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
        source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
        match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
        match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
        
        % Check statistics
        display(["target_known_nodes" length(unique(target_known_nodes)) length((target_known_nodes))])
        assert(length(unique(target_known_nodes))==length((target_known_nodes)) )
        
        display(["source_known_nodes" length(unique(source_known_nodes)) length((source_known_nodes))])
        assert(length(unique(source_known_nodes))==length((source_known_nodes)) )

        display(["match_source_nodes" length(unique(match_source_nodes)) length((match_source_nodes))])        
        assert(length(unique(match_source_nodes))==length((match_source_nodes)) )
        
        display(["match_target_nodes" length(unique(match_target_nodes)) length((match_target_nodes))])        
        assert(length(unique(match_target_nodes))==length((match_target_nodes)) )
        
        display(["labeled matched ratio" length(intersect(source_known_nodes,match_source_nodes))/length(match_source_nodes)])
        display(["labeled matched ratio" length(intersect(target_known_nodes,match_target_nodes))/length(match_target_nodes)])
        %% Reduce the dictionaries to matches
        dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
        dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
        
        y_s = f_source(source_known_nodes)';
        y_t = f_target(target_known_nodes)';    
        
        knownclasses = (unique([y_s; y_t]));
        
        RTarget = zeros(numofNodeTarget,length(knownclasses));
        % ONE VS ALL Trainning
        for perclass = 1 : length(knownclasses) 
             l = knownclasses(perclass); 
             % ONE-HOT ENCODING
             KnownLabel_source = (y_s == l); 
             KnownLabel_target = (y_t == l);
             
             RTarget(:,perclass) = Gralp( numofNodeSource,numofNodeTarget, ... % num of nodes
                             mu,gama_s,gama_t,... %hyperparameters
                             dict,W1,W2,... % dictionaries and graphs
                             KnownLabel_source,KnownLabel_target,... % known labels
                             source_known_nodes,target_known_nodes ); % indexes
        end
        %% SOFT TO HARD LABEL ASSIGNMENT
        predicted_labels = zeros(1,numofNodeTarget);
        for pernode = 1 : numofNodeTarget
            [~,lblidx] = max(RTarget(pernode,:));
            predicted_labels(pernode) =  knownclasses(lblidx);
        end
        
        target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
        acc_count = sum(predicted_labels(target_unknown_nodes) == f_target(target_unknown_nodes));
        errors_wda(iparam,itrial) = 100*(1-acc_count/ length(target_unknown_nodes));
    end
end

save ../../../3_outputs/COIL/match/errors_wda errors_wda
