%%  SVM: Source+ Target %%%%
        train_data=[source_data(:,source_known_nodes) target_data(:,target_known_nodes)];
        train_labels=[y_s ;y_t];
        
        svm_str=svmTrainOneAll(train_data', train_labels);
        test_data=target_data(:,target_unknown_nodes);
        pred_target=svmClassifyOneAll(svm_str, test_data');

        acc= sum((pred_target' - f_target(target_unknown_nodes)) == 0)/length(target_unknown_nodes);
        errors_svm_st(iparam,itrial)=100 - 100*acc;

%% NN: Source + Target %%%%%%
        train_data=[source_data(:,source_known_nodes) target_data(:,target_known_nodes)];
        train_labels=[y_s; y_t];
        test_data=target_data(:,target_unknown_nodes);
        
        pred_target=zeros(length(target_unknown_nodes),1);
        for itest=1:size(test_data,2)
            test_image=test_data(:,itest);
            d_vect=sum((train_data-repmat(test_image,1,size(train_data,2))).^2,1);
            [~, minind] =min(d_vect);
            pred_target(itest)=train_labels(minind);
        end
        
        acc = sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
        errors_nn_st(iparam,itrial)=100 - 100*acc;        
        
%% SA Classifier %%%%

        source_unknown_nodes = setdiff(1:size(source_data,2),source_known_nodes);

        pred_target = SAClassify(  source_data(:,source_known_nodes)',...
                     y_s, source_data(:,source_unknown_nodes)',...
                      target_data(:,target_known_nodes)', y_t, ...
                      target_data(:,target_unknown_nodes)', 57, 'nn');
        
        acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
        errors_sa(iparam,itrial)=100 - 100*acc;
%% Geodesic Flow Kernel - GFK
        pred_target = GFKClassify(  source_data(:,source_known_nodes)',...
                      y_s, source_data(:,source_unknown_nodes)',...
                      target_data(:,target_known_nodes)', y_t, ...
                      target_data(:,target_unknown_nodes)');
        acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
        errors_gfk(iparam,itrial)=100 - 100*acc;  

%% Semi Supervised Learning-SSL
if ~isempty(y_t)
    
    [fu,fu_CMN]=SSL(W2,target_known_nodes,target_unknown_nodes,f_target(target_known_nodes));
    [fu_hard_vals, fu_hard_labels]=max(fu,[],2);
    
    acc = sum(fu_hard_labels' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
    errors_ssl(iparam,itrial)=acc;
    
    [fuCMN_hard_vals, fuCMN_hard_labels]=max(fu_CMN,[],2);
    
    
    acc = sum(fuCMN_hard_labels' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
    errors_ssl_CMN(iparam,itrial)=100 - 100*acc;
end
% %% EasyAdaptPlus
% %         pred_target = EasyAdaptPlus(source_data(:,source_known_nodes)',...
% %                                y_s,target_data(:,target_known_nodes)', ...
% %                                y_t, target_data(:,target_unknown_nodes)', ...
% %                                1:10  );
% %         err=sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
% %         errors_ea(iparam,itrial)=err;
%         
%% LDADA
pred_target = LDADAClassify( source_data(:,source_known_nodes)', y_s, ...
    target_data(:,target_known_nodes)', y_t,...
    target_data(:,target_unknown_nodes)',10);
error_ldada=sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
errors_ldada(iparam,itrial)= 100 - 100*error_ldada;

%% SCA
if ~isempty(y_t)
    pred_target = SCAClassify( source_data(:,source_known_nodes)', y_s, ...
        target_data(:,target_known_nodes)', y_t,...
        target_data(:,target_unknown_nodes)', 0.02, 0.01, 9);
    acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
    errors_sca(iparam,itrial)=100 - 100*acc;
end
%% JGSA
% Set algorithm parameters
options.k = 38;            % #subspace bases
options.ker = 'gauss';     % kernel type, default='linear' options: linear, primal, gauss, poly

options.T = 15;             % #iterations, default=10

options.alpha= 0.83;           % the parameter for subspace divergence ||A-B||
options.mu = 0.065;             % the parameter for target variance
options.beta = 6;        % the parameter for P and Q (source discriminaiton)
options.kpar =  0.38;

pred_target = JGSAClassify(  source_data(:,source_known_nodes)',...
    y_s, target_data(:,target_known_nodes)', y_t,...
    target_data(:,target_unknown_nodes)',options);
acc=sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
errors_jgsa(iparam,itrial)=100 - 100*acc;

%% DASGA
sigma=1; %gausian weight parameter
k=3; %k nearest neighbour 

[W1_dasga,~,~] = CalculateWeightAndLabels_new(source,sigma,k);
numofNodeSource = length(f_source);

% Target
[W2_dasga,~,~] = CalculateWeightAndLabels_new(target,sigma,k);
numofNodeTarget = length(f_target);


DASGA_params.f_target = f_target'; %MODIFY ??
DASGA_params.f_source = f_source';
DASGA_params.mu = 1;
DASGA_params.N_eigenvectors = 10;
DASGA_params.N_iterations = 10;
DASGA_params.mu_2 = 1; 
DASGA_params.labels = unique(DASGA_params.f_source);
DASGA_params.labeled_target_indices = target_known_nodes;
DASGA_params.labeled_source_indices=source_known_nodes;
DASGA_params.W = W1_dasga;
DASGA_params.W_target = W2_dasga;

pred_target=DASGA( DASGA_params, 0);
acc=sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
errors_dasga(iparam,itrial)=100 - 100*acc;

