clear all
close all
clc

%% LOAD DATA
DataLoc = '../../../1_data/COIL';
load '../../../1_data/COIL/COIL_source.mat'
load '../../../1_data/COIL/COIL_target.mat'

s = [9,19,16,17,13,12,14,18,2,4];
t = [5,6,8,15,10,11,7,20,1,3];

data = [ COIL_source COIL_target];
for(i=1:10)
    source{i,1} = data{s(i)};
    target{i,1} = data{t(i)};
end
source_data = source;
target_data = target;

%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/COIL/target/NodeStruct.mat
load ../../../3_outputs/COIL/Dict_Source.mat
W_source = Dict;
load ../../../3_outputs/COIL/Dict_Target.mat
W_target = Dict;
numberofWavelets = 5;

addpath("../../wda_functions")
addpath(genpath("../../functions"))

paramlength = length(NodeStruct(1).targetknownnodes);

%% GENERATE GRAPHS AND LABELS
sigma = 0.2;
k = 5;

[W1,f_source,source_data] = CalculateWeightAndLabels_new(source_data,sigma,k);

numofNodeSource = length(f_source);

% Target
[W2,f_target,target_data] = CalculateWeightAndLabels_new(target_data,sigma,k);


numofNodeTarget = length(f_target);


%% Hyperparameters
mu =(10^(0)); 
gama_t =(10^(-1));
gama_s = (10^(-1));

%% Experiments
ntrial = length(NodeStruct);
for itrial = 1 : ntrial
    for iparam = 1 : paramlength
        display(["itrial" itrial "iparam" iparam])
        drawnow
        %% Get parameter setting
        target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
        source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
        match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
        match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
        
        %% Reduce the dictionaries to matches
        dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
        dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
        
        y_s = f_source(source_known_nodes)';
        y_t = f_target(target_known_nodes)';    

        % Check statistics
        display(["target_known_nodes" length(unique(target_known_nodes)) length((target_known_nodes))])
        assert(length(unique(target_known_nodes))==length((target_known_nodes)) )
        
        display(["source_known_nodes" length(unique(source_known_nodes)) length((source_known_nodes))])
        assert(length(unique(source_known_nodes))==length((source_known_nodes)) )

        display(["match_source_nodes" length(unique(match_source_nodes)) length((match_source_nodes))])        
        assert(length(unique(match_source_nodes))==length((match_source_nodes)) )
        
        display(["match_target_nodes" length(unique(match_target_nodes)) length((match_target_nodes))])        
        assert(length(unique(match_target_nodes))==length((match_target_nodes)) )
        
        display(["labeled matched ratio" length(intersect(source_known_nodes,match_source_nodes))/length(match_source_nodes)])
        assert(isempty(intersect(source_known_nodes,match_source_nodes)))

        display(["labeled matched ratio" length(intersect(target_known_nodes,match_target_nodes))/length(match_target_nodes)])        
        assert(isempty(intersect(target_known_nodes,match_target_nodes)))
        
        %% Run others
        target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
        run("../runothermethods.m")


    end
end

% save ../../../3_outputs/COIL/target/errors_svm_st errors_svm_st
% save ../../../3_outputs/COIL/target/errors_nn_st errors_nn_st
% save ../../../3_outputs/COIL/target/errors_sa errors_sa
% save ../../../3_outputs/COIL/target/errors_gfk errors_gfk
% save ../../../3_outputs/COIL/target/errors_ssl errors_ssl
% save ../../../3_outputs/COIL/target/errors_ssl_CMN errors_ssl_CMN
% save ../../../3_outputs/COIL/target/errors_ldada errors_ldada
% save ../../../3_outputs/COIL/target/errors_sca errors_sca
% save ../../../3_outputs/COIL/target/errors_jgsa errors_jgsa
save ../../../3_outputs/COIL/target/errors_dasga errors_dasga
