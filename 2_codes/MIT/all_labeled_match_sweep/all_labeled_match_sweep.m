clear all
close all
clc

%% LOAD DATA
first_pose= 9; % Frontal view
second_pose= 6; % Profile view

MIT_data = struct2cell(importdata( "../../../1_data/MIT/poseFile.mat"));
MIT_source = MIT_data{first_pose};
MIT_target = MIT_data{second_pose};

%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/MIT/all_labeled_match/NodeStruct.mat
load ../../../3_outputs/MIT/Dict_Source.mat
W_source = Dict;
load ../../../3_outputs/MIT/Dict_Target.mat
W_target = Dict;
numberofWavelets = 5;

addpath("../../wda_functions")
paramlength = length(NodeStruct(1).targetknownnodes);

%% GENERATE GRAPHS AND LABELS
sigma = 0.2;
k = 5;

[W1,f_source,source_data] = CalculateWeightAndLabels_new(MIT_source,sigma,k);
% [W1_n,f_source_n,source_data_n] = CalculateWeightAndLabels(MIT_source,sigma,k);
% isequal(W1,W1_n)
% isequal(f_source,f_source_n)
% isequal(source_data,source_data_n)

numofNodeSource = length(f_source);

% Target
[W2,f_target,target_data] = CalculateWeightAndLabels_new(MIT_target,sigma,k);
% [W2_n,f_target_n,target_data_n] = CalculateWeightAndLabels(MIT_target,sigma,k);
% isequal(W2,W2_n)
% isequal(f_target,f_target_n)
% isequal(target_data,target_data_n)


numofNodeTarget = length(f_target);


%% Hyperparameters
mu =(10^(0)); 
gama_t =(10^(-1));
gama_s = (10^(-1));

%% Experiments
ntrial = length(NodeStruct);
errors_wda = zeros(paramlength,ntrial)
for itrial = 1 : ntrial
    for iparam = 1 : paramlength
        display(["itrial" itrial "iparam" iparam])
        drawnow
        %% Get parameter setting
        target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
        source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
        match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
        match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
        
        %% Reduce the dictionaries to matches
        dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
        dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
        
        y_s = f_source(source_known_nodes)';
        y_t = f_target(target_known_nodes)';    
        
        knownclasses = (unique([y_s; y_t]));
        
        RTarget = zeros(numofNodeTarget,length(knownclasses));
        % ONE VS ALL Trainning
        for perclass = 1 : length(knownclasses) 
             l = knownclasses(perclass); 
             % ONE-HOT ENCODING
             KnownLabel_source = (y_s == l); 
             KnownLabel_target = (y_t == l);
             
             RTarget(:,perclass) = Gralp( numofNodeSource,numofNodeTarget, ... % num of nodes
                             mu,gama_s,gama_t,... %hyperparameters
                             dict,W1,W2,... % dictionaries and graphs
                             KnownLabel_source,KnownLabel_target,... % known labels
                             source_known_nodes,target_known_nodes ); % indexes
        end
        %% SOFT TO HARD LABEL ASSIGNMENT
        predicted_labels = zeros(1,numofNodeTarget);
        for pernode = 1 : numofNodeTarget
            [~,lblidx] = max(RTarget(pernode,:));
            predicted_labels(pernode) =  knownclasses(lblidx);
        end
        
        target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
        acc_count = sum(predicted_labels(target_unknown_nodes) == f_target(target_unknown_nodes));
        errors_wda(iparam,itrial) = 100*(1-acc_count/ length(target_unknown_nodes));
        errors_wda(iparam,itrial)
    end
end

save ../../../3_outputs/MIT/all_labeled_match/errors_wda errors_wda
