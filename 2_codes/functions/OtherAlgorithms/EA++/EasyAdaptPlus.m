function [ results ] = EasyAdaptPlus(labeled_data_src, labels_src, labeled_data_trgt, labels_trgt, unlabeled_data_trgt, labels  )

N_unlabeled_data = size(unlabeled_data_trgt,1);

phi_s = [repmat(labeled_data_src, 1, 2) zeros(size(labeled_data_src))];
phi_t = [labeled_data_trgt zeros(size(labeled_data_trgt)) labeled_data_trgt];
phi_u = [zeros(size(unlabeled_data_trgt)) unlabeled_data_trgt -unlabeled_data_trgt];
phi_tu = [unlabeled_data_trgt zeros(size(unlabeled_data_trgt)) unlabeled_data_trgt];

training_data = [phi_s; phi_t; repmat(phi_u, length(labels), 1)];
training_labels = zeros(length(labels_src) + length(labels_trgt) + length(labels)*size(unlabeled_data_trgt, 1), 1);
training_labels(1:length(labels_src),1) = labels_src;
training_labels(length(labels_src)+1:length(labels_src)+size(labeled_data_trgt, 1), 1) = labels_trgt;

temp = zeros(length(labels)*N_unlabeled_data, 1);
for ii=1:length(labels)
    temp((ii-1)*N_unlabeled_data+1:ii*N_unlabeled_data, 1) = labels(ii)*ones(N_unlabeled_data,1);
end

training_labels(length(labels_src)+size(labeled_data_trgt, 1)+1:end, 1) = temp;


svmStructList = svmTrainOneAll( training_data, training_labels ); 
results = svmClassifyOneAll( svmStructList, phi_tu );


end


