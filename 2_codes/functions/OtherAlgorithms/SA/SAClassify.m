function [ labels_target_unlabeled ] = SAClassify(  data_source_labeled, labels_source, data_source_unlabeled,...
                                                      data_target_labeled, labels_target, data_target_unlabeled, d, option)
% Domain adaptation with "Unsupervised Visual Domain Adaptation Using
% Subspace Alignment", Fernando et al. ICCV 2013
%
% INPUTS:
% data_source_labeled: Labeled source data, each row is a data sample
% labels_source: Labels of data_source_labeled, each row is a label
% data_source_unlabeled: Unlabeled source data, each row is a  data sample
% data_target_labeled: Labeled target data, each row is a data sample
% labels_target: Labels of data_target_labeled, each row is a label
% data_target_unlabeled: Unlabeled target data, each row is a data sample
% d: dimension of the PCA embedding
%
% OUTPUTS:
% labels_target_unlabeled: Estimated labels of unlabeled target data



%epsilon=1e-8; % Numerical zero

data_source_labeled=data_source_labeled';
data_source_unlabeled=data_source_unlabeled';
data_target_labeled=data_target_labeled';
data_target_unlabeled=data_target_unlabeled';

data_source=[data_source_labeled data_source_unlabeled];
data_target=[data_target_labeled data_target_unlabeled];

class_labels_source=unique(labels_source);
class_labels_target=unique(labels_target);
class_labels=unique([class_labels_source; class_labels_target]);
num_classes=length(class_labels);

num_source_labeled=size(data_source_labeled,2);
num_source_unlabeled=size(data_source_unlabeled,2);
num_source_samples=num_source_labeled+num_source_unlabeled;
num_target_labeled=size(data_target_labeled,2);
num_target_unlabeled=size(data_target_unlabeled,2);
num_target_samples=num_target_labeled+num_target_unlabeled;




% Find PCA bases of source and target domains
mean_source=mean(data_source,2);
mean_target=mean(data_target,2);

source_mshifted=data_source-repmat(mean_source,1,num_source_samples);
target_mshifted=data_target-repmat(mean_target,1,num_target_samples);

[Xs Ds]=eig(source_mshifted*source_mshifted');
[~, sort_ind]=sort(diag(Ds),'descend');
Xs=Xs(:,sort_ind);

[Xt Dt]=eig(target_mshifted*target_mshifted');
[~, sort_ind]=sort(diag(Dt),'descend');
Xt=Xt(:,sort_ind);

% figure; stem(source_data(:,5)'*Xs)
% figure; stem(target_data(:,82)'*Xt)

% Select first d PCA basis vectors
Xs=Xs(:,1:d);
Xt=Xt(:,1:d);

% Find projection matrix to transform from source to target
P=Xs*Xs'*Xt*Xt';

trans_data_source_labeled=P'*(data_source_labeled - repmat(mean_source, 1, num_source_labeled ))+repmat(mean_target, 1, num_source_labeled);
    
% Put together all labeled data
all_train_data=[trans_data_source_labeled data_target_labeled];
all_train_labels=[labels_source' labels_target'];

switch(option)
    case {'svm'}
        % Train SVM with all labeled data
        svmstr=svmTrainOneAll(all_train_data',all_train_labels');
        
        % Estimate labels of unlabeled target data
        labels_target_unlabeled=svmClassifyOneAll( svmstr, data_target_unlabeled');
        
    case {'nn'}
        
        % Nearest-neighbor classification
        labels_target_unlabeled=zeros(num_target_unlabeled,1);
        for itest=1:num_target_unlabeled
            testdata=data_target_unlabeled(:,itest);
            [~, minind]=min( sum(  (all_train_data - repmat(testdata,1,num_target_labeled+num_source_labeled)).^2, 1) );
            labels_target_unlabeled(itest)=all_train_labels(minind);
        end
        
end

