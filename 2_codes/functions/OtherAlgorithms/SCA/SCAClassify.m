function [ labels_target_unlabeled ] = SCAClassify(  data_source_labeled, labels_source, ...
                                                      data_target_labeled, labels_target, data_target_unlabeled, beta, delta, d)
 % Domain adaptation with "Scatter Component Analysis: A unified framework
% for domain adaptation and domain generalization", Ghifary et al. 
% IEEE PAMI 2017
%
% INPUTS:
% data_source_labeled: Labeled source data, each row is a data sample
% labels_source: Labels of data_source_labeled, each row is a label
% data_source_unlabeled: Unlabeled source data, each row is a  data sample
% data_target_labeled: Labeled target data, each row is a data sample
% labels_target: Labels of data_target_labeled, each row is a label
% data_target_unlabeled: Unlabeled target data, each row is a data sample
% beta: Algorithm weight hyperparameter (typical range 0.1-1)
% delta: Algorithm weight hyperparameter (typical range 10-1e6)
% d: Algorithm dimension hyperparameter (typical range 5-20)
%
% OUTPUTS:
% labels_target_unlabeled: Estimated labels of unlabeled target data
%
% CONDITIONS: Class indices must be positive integers



epsilon = 0.00001;

X_train_cell=cell(2,1);
X_train_cell{1,1}=data_source_labeled;
X_train_cell{2,1}=data_target_labeled;

Y_train_cell=cell(2,1);
Y_train_cell{1,1}=labels_source;
Y_train_cell{2,1}=labels_target;


X_s = [data_source_labeled; data_target_labeled];
Y_s = [labels_source; labels_target];


dist_s = pdist2(X_s, X_s);
dist_s = dist_s.^2;


sigma = power( median(dist_s(:) ), 2);

K = exp(-dist_s./(2 * sigma * sigma));


% Learn SCA model
[P, T, D, Q, K_bar] = SCA_quantities(K, X_train_cell, Y_train_cell);
[B, A] = SCA_transformation(P, T, D, Q, K_bar, beta, delta, epsilon);



% Apply on test data
X_t=data_target_unlabeled;

n_s = size(X_s, 1);
n_t = size(X_t, 1);

H_s = eye(n_s) - ones(n_s)./n_s;
K_s = H_s * K * H_s;

dist_t = pdist2(X_s, X_t);
dist_t = dist_t.^2;

K_t = exp(-dist_t./(2 * sigma * sigma));
H_t = eye(n_t) - ones(n_t)./n_t;
K_t = H_s * K_t * H_t;


vals = diag(A);
ratio = [];
count = 0;
for i = 1:length(vals)
    if vals(i)<0
        break;
    end
    count = count + vals(i);
    ratio = [ratio; count];
    vals(i) = 1/sqrt(vals(i));
end
A_sqrt = diag(vals);
ratio = ratio/count;


n_eigs=d;

Zt = K_t' * B(:, 1:n_eigs) * A_sqrt(1:n_eigs, 1:n_eigs);
Zs = K_s' * B(:, 1:n_eigs) * A_sqrt(1:n_eigs, 1:n_eigs);

dist_Zs_Zt= pdist2(Zs, Zt);
[~, minind]=min(dist_Zs_Zt);
labels_target_unlabeled=Y_s(minind);



end