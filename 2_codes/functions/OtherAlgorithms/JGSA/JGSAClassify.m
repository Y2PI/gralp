function [ labels_target_unlabeled ] = JGSAClassify(  data_source_labeled, labels_source, ...
                                                      data_target_labeled, labels_target,...
                                                      data_target_unlabeled,options)
                                                  
% Zhang, J., Li, W., & Ogunbona, P. (2017). Joint geometrical and statistical alignment for 
% visual domain adaptation. Proceedings - 30th IEEE Conference on Computer Vision and Pattern 
% Recognition, CVPR 2017, 2017�Janua, 5150�5158. https://doi.org/10.1109/CVPR.2017.547
%
% INPUTS:
% data_source_labeled   : Labeled source data, each row is a data sample
% labels_source         : Labels of data_source_labeled, each row is a label
% data_source_unlabeled : Unlabeled source data, each row is a  data sample
% data_target_labeled   : Labeled target data, each row is a data sample
% labels_target         : Labels of data_target_labeled, each row is a label
% data_target_unlabeled : Unlabeled target data, each row is a data sample
% options               : struct which contains algorithm parameters  
%    options.k             % subspace base dimension k - [20, 180]
%    options.ker           % kernel type, default='linear' options: linear, primal, gauss, poly
%    options.T             % #iterations, default=10
%    options.alpha         % the parameter for subspace divergence ||A-B||  (lambda in the paper, lambda = 1 is sufficient)  
%    options.mu            % the parameter for target variance (mu =1 is sufficient)
%    options.beta          % the parameter for P and Q (source discriminaiton)  (beta [2^-15, 0.5]) 
%    options.kpar          % the parameter for kernel
%                               if kernel 'poly'  kpar is the order of the polynomial
%                               if kernel 'gauss' kpar is gamma value in gaussian kernel

%
% OUTPUTS:
% labels_target_unlabeled: Estimated labels of unlabeled target data
%


Xs = data_source_labeled.'; % each column is a sample
Xt = [data_target_labeled.' data_target_unlabeled.' ];

Ys = labels_source';
Yt = labels_target';

%% Nearest Neighbour 
labeled_data = [Xs.';Xt(:,1:length(labels_target)).'];
labels = [Ys Yt];
dist_train_test= pdist2(labeled_data, Xt(:,(length(labels_target)+1:end)).');
[~, minind]=min(dist_train_test);
Yt0 = [Yt labels(minind)];
%% JGSA
[Zs, Zt, A, Att] = JGSA(Xs, Xt, Ys, Yt0,Yt, options);
%% Nearest Neighbour
labeled_data = [Zs.';Zt(:,1:length(labels_target)).'];
labels = [Ys Yt];
dist_train_test= pdist2(labeled_data, Zt(:,(length(labels_target)+1:end)).');
[~, minind]=min(dist_train_test);
labels_target_unlabeled=(labels(minind))';
end