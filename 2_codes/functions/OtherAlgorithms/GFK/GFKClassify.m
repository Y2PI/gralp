function [ labels_target_unlabeled ] = GFKClassify(  data_source_labeled, labels_source, data_source_unlabeled,...
                                                      data_target_labeled, labels_target, data_target_unlabeled)
% Domain adaptation with "Geodesic Flow Kernel for Unsupervised Domain
% Adaptation", Gong et al. CVPR 2012
%
% INPUTS:
% data_source_labeled: Labeled source data, each row is a data sample
% labels_source: Labels of data_source_labeled, each row is a label
% data_source_unlabeled: Unlabeled source data, each row is a  data sample
% data_target_labeled: Labeled target data, each row is a data sample
% labels_target: Labels of data_target_labeled, each row is a label
% data_target_unlabeled: Unlabeled target data, each row is a data sample
% 
% OUTPUTS:
% labels_target_unlabeled: Estimated labels of unlabeled target data
%
% CONDITIONS:
% Source and target data need to be of the same dimension



data_source_labeled=data_source_labeled';
data_source_unlabeled=data_source_unlabeled';
data_target_labeled=data_target_labeled';
data_target_unlabeled=data_target_unlabeled';

data_source=[data_source_labeled data_source_unlabeled];
data_target=[data_target_labeled data_target_unlabeled];

class_labels_source=unique(labels_source);
class_labels_target=unique(labels_target);
class_labels=unique([class_labels_source; class_labels_target]);
num_classes=length(class_labels);

num_source_labeled=size(data_source_labeled,2);
num_source_unlabeled=size(data_source_unlabeled,2);
num_source_samples=num_source_labeled+num_source_unlabeled;
num_target_labeled=size(data_target_labeled,2);
num_target_unlabeled=size(data_target_unlabeled,2);
num_target_samples=num_target_labeled+num_target_unlabeled;




% Find PCA bases of source and target domains
mean_source=mean(data_source,2);
mean_target=mean(data_target,2);

source_mshifted=data_source-repmat(mean_source,1,num_source_samples);
target_mshifted=data_target-repmat(mean_target,1,num_target_samples);

[Xs, Ds]=eig(source_mshifted*source_mshifted');
[~, sort_ind]=sort(diag(Ds),'descend');
Xs=Xs(:,sort_ind);
source_pca_eigs=diag(Ds);
source_pca_eigs=source_pca_eigs(sort_ind);


[Xt, Dt]=eig(target_mshifted*target_mshifted');
[~, sort_ind]=sort(diag(Dt),'descend');
Xt=Xt(:,sort_ind);
target_pca_eigs=diag(Dt);
target_pca_eigs=target_pca_eigs(sort_ind);


% figure; stem(source_data(:,5)'*Xs)
% figure; stem(target_data(:,82)'*Xt)

% Select number of dimensions 

total_eng=sum(source_pca_eigs);
sum_eigs=0;
for ieig=1:length(source_pca_eigs)
    sum_eigs=sum_eigs+source_pca_eigs(ieig);
    if(sum_eigs>=total_eng*0.98)
        dsource=ieig;
        break;
    end
end

total_eng=sum(target_pca_eigs);
sum_eigs=0;
for ieig=1:length(target_pca_eigs)
    sum_eigs=sum_eigs+target_pca_eigs(ieig);
    if(sum_eigs>=total_eng*0.98)
        dtarget=ieig;
        break;
    end
end

d=min([dsource dtarget size(Xs,1)/2]);

G=GFK(Xs, Xt(:,1:d));

data_labeled_mshifted=[data_source_labeled-repmat(mean_source,1,num_source_labeled)  data_target_labeled-repmat(mean_target,1,num_target_labeled)];
data_labels=[labels_source' labels_target'];


data_target_unlabeled_mshifted=  data_target_unlabeled-repmat(mean_target,1,num_target_unlabeled);

sim_matrix=data_target_unlabeled_mshifted'*G*data_labeled_mshifted;
dist_matrix=repmat(diag(data_target_unlabeled_mshifted'*G*data_target_unlabeled_mshifted), 1, size(data_labeled_mshifted, 2) )...
    +   repmat(diag(data_labeled_mshifted'*G*data_labeled_mshifted)', size(data_target_unlabeled_mshifted, 2), 1) ...
    - 2*sim_matrix;
    
[~, mininds]=min(dist_matrix, [], 2);
labels_target_unlabeled=data_labels(mininds);
labels_target_unlabeled=labels_target_unlabeled';


end
