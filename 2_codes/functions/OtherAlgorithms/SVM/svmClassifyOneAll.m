function [ Y ] = svmClassifyOneAll( svmStructList, X )
% Classifies test images in X with respect to the learned multiple class
% models in svmStructList
% Input X: Data matrix, each row of X is a data sample
% Output Y: i-th row of Y is the estimated class label of the i-th data
% sample. 

num_data=size(X,1);
num_classes=length(svmStructList);


Y=zeros(num_data,1);

class_matrix=zeros(num_data,num_classes);
score_matrix=zeros(num_data,num_classes);    

for iclass=1:num_classes
    if(isempty(svmStructList{iclass, 1}))
        class_matrix(:,iclass)=0;
        score_matrix(:,iclass)=NaN;
    else
        [labels, scores]=predict(svmStructList{iclass},X);
        class_matrix(:,iclass)=labels; 
        score_matrix(:,iclass)=abs(scores(:,1));
    end
end

[R, C]=find(class_matrix==1);
Y(R)=C;

failed_samp_ind=find(all(class_matrix~=1, 2));

scores_failed_samples=score_matrix(failed_samp_ind,:);
[~, class_failed_samples]=min(scores_failed_samples,[], 2); 

Y(failed_samp_ind)=class_failed_samples;
end

