function [ svmStructList ] = multisvmtrain( X, Y )
% Trains SVM for multiple classes

class_names=unique(Y);
num_classes=length(class_names);
svmStructList=cell(num_classes,num_classes);

for iclass=1:num_classes

    Y_iclass=find(Y==class_names(iclass));
    X_iclass=X(Y_iclass,:);
    
    for jclass=iclass+1:num_classes
        Y_jclass=find(Y==class_names(jclass));
        X_jclass=X(Y_jclass,:);
        
        X_ij=[X_iclass; X_jclass];
        Y_ij=[iclass*ones(size(Y_iclass)); jclass*ones(size(Y_jclass))];
        options.MaxIter = 1e6;
        svmStructList{iclass,jclass}=svmtrain(X_ij,Y_ij, 'kktviolationlevel', 0.05, 'boxconstraint', 0.8, 'tolkkt', 5e-3, 'Options', options);
    end
end

end
