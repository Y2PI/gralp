function [ svmStructList ] = svmTrainOneAll( X, Y )
% Trains SVM for multiple classes
% Input X: contains a data sample in each row
% Input Y: contains the class labels of i-th data sample in i-th row
% Class labels must be positive integers
% Output svmStructList: list containing a hypothesis for each class


if(sum(Y<1))
    disp('All labels must be positive integers');
    return
end

num_classes=max(Y);

svmStructList=cell(num_classes,1);

for iclass=1:num_classes
    ind_class=find(Y==iclass);
    
    if(isempty(ind_class))
        continue;
    end
    
    ind_oth_class=find(Y~=iclass);
    
    X_class=X(ind_class,:);
    X_oth_class=X(ind_oth_class,:);
     
    X_train=[X_class; X_oth_class];
    Y_train=[ones(length(ind_class),1); -1*ones(length(ind_oth_class),1)];
        
    svmStructList{iclass,1}=fitcsvm(X_train,Y_train);
        
end


% 
% for iclass=1:num_classes
% 
%     Y_iclass=find(Y==class_names(iclass));
%     X_iclass=X(Y_iclass,:);
%     
%     for jclass=iclass+1:num_classes
%         Y_jclass=find(Y==class_names(jclass));
%         if(isempty(Y_iclass) || isempty(Y_jclass))
%             continue;
%         end
%         X_jclass=X(Y_jclass,:);
%         
%         X_ij=[X_iclass; X_jclass];
%         Y_ij=[iclass*ones(size(Y_iclass)); jclass*ones(size(Y_jclass))];
%         svmStructList{iclass,jclass}=svmtrain(X_ij,Y_ij);
%     end
% end

end
