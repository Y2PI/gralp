function [ Y ] = multisvmclassify( svmStructList, X )
% Classifies test images in X with respect to the learned multiple class
% models in svmStructList

num_data=size(X,1);
num_classes=length(svmStructList);


Y=zeros(num_data,1);

for idata=1:num_data
    class_matrix=zeros(num_classes,num_classes);

    for iclass=1:num_classes
        for jclass=iclass+1:num_classes
            class_matrix(iclass,jclass)=svmclassify(svmStructList{iclass, jclass},X(idata, :));            
        end
    end
    
    num_counts=zeros(num_classes,1);
    for iclass=1:num_classes
       num_counts(iclass)=sum(sum(class_matrix==iclass)); 
    end
    
    [maxval, maxind]=max(num_counts);
    Y(idata)=maxind;
end



end

