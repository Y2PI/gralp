
function [fu,fu_CMN]=SSL(Weight,target_known_nodes,target_unknown_nodes,target_known_labels)
f_unique = unique(target_known_labels);
fl = zeros(length(f_unique));
for ind = 1:length(target_known_labels) 
    fl(ind,find(f_unique==target_known_labels(ind))) = 1;
end
old_ind = 1:(length(target_known_nodes)+length(target_unknown_nodes));
new_ind =[  target_known_nodes target_unknown_nodes] ;
M = eye(length(Weight));
for i = 1:length(old_ind)
        k=old_ind(i);
        l=new_ind(i);
    if(old_ind(i)~=new_ind(i))
    
        
        M(k,k) = 0;
        M(l,k)=1;
    end
end
WA = M'*Weight*M;

[fu_, fu_CMN_] = harmonic_function(WA, fl);
fu = ones(length(fu_),10);
fu_CMN = zeros(length(fu_),10);
for(i = 1 : length(f_unique))
    fu(:,f_unique(i)) = fu_(:,i);
    fu_CMN(:,f_unique(i)) = fu_CMN_(:,i);
end  
end

