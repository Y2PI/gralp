function [ output ] = FindTargetLabels(alpha_s, y_t, S_t, U_t, mu )

a = mu*eye(length(alpha_s)) + U_t'*(S_t'*S_t)*U_t;
b = mu*alpha_s + U_t'*S_t'*y_t;
alpha_t = a\b;
% f_target_hat = (V'*(M'*M)*V*lambda)\(f_hat+V'*M'*labels*lambda);
output = transpose(alpha_t'*U_t');

end

