clear all;
close all;
clc;

addpath ../Data
addpath('./functions')
addpath(genpath('./functions'))
warning('off','all');

generate_setup=0;

%load('../Data/random_indices');
%data = ['10P_S36l1p_T36l2p_LR'; '10P_S36l1p_T36l5p_LR'; '10P_S36l1p_T36l9p_LR'];
%data = '10P_S36l1p_T36l5p_LR';

DASGA_active = 1; % Done
SVM_active = 0; % Done
EA_active = 0; % Done
Dama_active = 0;  % Done
SSL_active = 0; % Done
SA_active = 0; % Done
NN_active = 0; % Done
GFK_active = 0; % Done

SCA_active=0; %Done
JGSA_active=0; %Done
LDADA_active=0; % Done

% optimization parameter
mu = 0.01; 
mu_2 = 0.85;
N_NearestNeighbors = 38;





if(Dama_active == 1)
    mu_Dama = 1;
    dim_Dama = 10;
    sigma_Dama=0.03;
end


if(SA_active==1)
    dim_SA = 8;
end



if(SCA_active)
    dim_SCA= 10;
    beta_SCA= 0.0001;
    delta_SCA=0.01;
end


if(JGSA_active)
    JGSA_options.k=10; % dimension
    JGSA_options.ker='linear';
    JGSA_options.T=10; %number of iterations
    JGSA_options.beta=0.0001; % source discrimination
    JGSA_options.alpha=1; % ||A-B||
    JGSA_options.mu=1; % target variance
    JGSA_options.kpar=1;
end

N_simulations = 10; %Number of simulations
N_eigenvectors =  9;
N_iterations = 10;
N_train_vect = 5:5:60;
% N_source_members = 360;

GFK_Error = zeros(length(N_train_vect), 1);
DASGA_Error = zeros(length(N_train_vect), 1);
Dama_Error = zeros(length(N_train_vect), 1);
EA_Error = zeros(length(N_train_vect), 1);
SVM_Error = zeros(length(N_train_vect), 1);
SSL_Error1_target = zeros(length(N_train_vect), 1);
SSL_Error2_target = zeros(length(N_train_vect), 1);
SSL_Error1_all = zeros(length(N_train_vect), 1);
SSL_Error2_all = zeros(length(N_train_vect), 1);
SA_Error = zeros(length(N_train_vect), 1);
Err_NN = zeros(length(N_train_vect), 1);
% New methods
SCA_Error= zeros(length(N_train_vect), 1);
JGSA_Error= zeros(length(N_train_vect), 1);
LDADA_Error= zeros(length(N_train_vect), 1);


% N_classes = numOfPeople;
% N_class_member_src = numOfPoseParams_src*numOfLightParams_src;
% N_class_member_trgt = numOfPoseParams_trgt*numOfLightParams_trgt;

N_classes=10;
N_class_member_src=36;
N_class_member_trgt=36;

% Create labels with mdscale algoritm
% labels_DASGA = CreateLabels(distance, N_classes, N_class_member_src);
% labels = [-N_classes:2:-2 2:2:N_classes]';
% labels = transpose(1:N_classes);


% % Choose random indices for source and target domain images
% image_selection_indices_target = RandomIndexSelection(N_classes, N_class_member_src, N_class_member_trgt);
% image_selection_indices_source = RandomIndexSelection(N_classes, N_class_member_trgt, N_class_member_src);
% image_selection_indices_all = [image_selection_indices_source size(image_matrix,1)+image_selection_indices_target];
%
% % Order images at each classes randomly in source and target domains
% images_target = image_matrix_target(image_selection_indices_target, :);
% images_source = image_matrix(image_selection_indices_source, :);
% distances_target = distance_target(image_selection_indices_target, image_selection_indices_target);
% distances_source = distance(image_selection_indices_source, image_selection_indices_source);
% distances_all = distance_all(image_selection_indices_all, image_selection_indices_all);

%images_source=image_matrix;
%images_target=image_matrix_target;





N_source_members = N_class_member_src * N_classes;
N_target_members = N_class_member_trgt * N_classes;

if(generate_setup)
    load('../Data/10P_S36l1p_T36l9p_LR');
    
    % Downsample original images
    downsamp_fact=1;
    new_size=100*downsamp_fact;
    images_source=zeros(N_source_members,new_size^2);
    images_target=zeros(N_target_members,new_size^2);
    
    for iimg=1:N_source_members
        source_img= reshape(image_matrix(iimg,:),100,100);
        source_img=imresize(source_img,downsamp_fact);
        source_img=source_img(:);
        source_img=source_img/norm(source_img,'fro');
        images_source(iimg,:)=source_img';
    end
    
    for iimg=1:N_target_members
        target_img= reshape(image_matrix_target(iimg,:),100,100);
        target_img=imresize(target_img,downsamp_fact);
        target_img=target_img(:);
        target_img=target_img/norm(target_img,'fro');
        images_target(iimg,:)=target_img';
    end
    
    save ../Data/images_source.mat images_source
    save ../Data/images_target.mat images_target
    
    % Random index selection
    trials=cell(1,length(N_train_vect));
    N_sim_max=100;
    for itrials=1:length(N_train_vect)
        N_train=N_train_vect(itrials);
        inds=zeros(N_sim_max,N_train);
        for isim=1:N_sim_max
            inds(isim,:)=randperm(N_target_members, N_train);
        end
        trials{itrials}=inds;
    end
    save ../Data/trials.mat trials
    
else
    load ../Data/images_source.mat
    load ../Data/images_target.mat
    load ../Data/trials.mat
end

distances_source=pdist2(images_source,images_source);
distances_target=pdist2(images_target,images_target);
labels_DASGA = CreateLabels(distances_source, N_classes, N_class_member_src);


% Assign labels to all members in target and source domain
f_source_DASGA = repmat(labels_DASGA', N_class_member_src, 1);
f_source_DASGA = f_source_DASGA(:);
% f = f(image_selection_indices_source);

f_target_DASGA = repmat(labels_DASGA', N_class_member_trgt, 1);
f_target_DASGA = f_target_DASGA(:);
%f_target = f_target(image_selection_indices_target);

%N_target_members = length(f_target_DASGA);
%face_recog_random_indices = ceil(random_indices*N_target_members);

labels=1:N_classes;
f_source= repmat(labels, N_class_member_src, 1);
f_source = f_source(:);
f_target = repmat(labels, N_class_member_trgt, 1);
f_target = f_target(:);


% % % % % % % %%% ERASE:
% N_simulations= 5;
% % % % % % % %%%%%

% Repeat simulations and take average of results
for n_t =1:length(N_train_vect)
    
    n_t
    
    %N_train = N_train_vect(nt);
    
    % N_test = N_class_member_trgt-N_train;
    %N_target_unlabeled_members = N_target_members-N_train;
    
    
    N_known_target_labels= N_train_vect(n_t);
    N_target_unlabeled_members = N_target_members - N_known_target_labels;
    
    
    %cost = zeros(N_iterations+1, 1);
    %Error = zeros(N_iterations+2, 1);
    
    for ns=1:N_simulations
        %labeled_target_indices = face_recog_random_indices(nt, ns1, 1:N_train);
        
        inds=trials{n_t}(ns,:);
        %labeled_target_indices = labeled_target_indices(:);
        %         labeled_target_indices = randperm(N_target_members, N_train);
        
        
        
        known_target_inds = sort(inds);
        unknown_target_inds = setdiff(1:length(f_target), known_target_inds);
        known_source_inds = 1:length(f_source);
        unknown_source_inds = setdiff(1:length(f_source), known_source_inds);
        
        known_target_data = images_target(known_target_inds, :);
        known_target_labels = f_target(known_target_inds);
        unknown_target_data = images_target(unknown_target_inds, :);
        unknown_target_labels = f_target(unknown_target_inds);
        
        known_source_data = images_source(known_source_inds, :);
        known_source_labels = f_source(known_source_inds);
        unknown_source_data=[];
        
        
        if DASGA_active == 1
            DASGA_params.N_simulations = N_simulations;
            DASGA_params.f_target = f_target_DASGA;
            DASGA_params.f_source = f_source_DASGA;
            DASGA_params.mu = mu;
            DASGA_params.N_eigenvectors = N_eigenvectors;
            DASGA_params.N_iterations = N_iterations;
            DASGA_params.mu_2 = mu_2;
            DASGA_params.labels = unique(DASGA_params.f_source);
            DASGA_params.labeled_target_indices = known_target_inds;
            DASGA_params.W = full(adjacency_weighted(distances_source, N_NearestNeighbors, 1));
            DASGA_params.W_target = full(adjacency_weighted(distances_target, N_NearestNeighbors, 1));
            [  DASGA_Error(n_t,1) ] = DASGA_Error(n_t,1) + (1/N_simulations)*DASGA( DASGA_params, 0);
        end
        
        if(JGSA_active)
            estimated_target_labels =JGSAClassify(  known_source_data, known_source_labels, ...
                known_target_data, known_target_labels, unknown_target_data,JGSA_options);
            
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            JGSA_Error(n_t)=JGSA_Error(n_t)+ (1/N_simulations)*Error;
        end       
                
        if SA_active == 1
            estimated_target_labels = SAClassify( known_source_data, known_source_labels, unknown_source_data, ...
                known_target_data, known_target_labels, unknown_target_data, dim_SA, 'nn');
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            SA_Error(n_t, 1) = SA_Error(n_t, 1) + (1/N_simulations)*Error;
        end
        
        
        if(SCA_active)
            estimated_target_labels = SCAClassify(  known_source_data, known_source_labels,...
                known_target_data, known_target_labels, unknown_target_data, beta_SCA, delta_SCA, dim_SCA);
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            SCA_Error(n_t)=SCA_Error(n_t)+ (1/N_simulations)*Error;
        end
        
        
        if (Dama_active == 1)
            estimated_target_labels = DamaClassify(  known_source_data, known_source_labels, [], known_target_data, known_target_labels, unknown_target_data, ...
                mu_Dama, dim_Dama, sigma_Dama);
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            Dama_Error(n_t,1) =  Dama_Error(n_t,1) + (1/N_simulations)*Error;
        end
        
                
        if(LDADA_active)
            estimated_target_labels =LDADAClassify (  known_source_data, known_source_labels, ...
                known_target_data, known_target_labels, unknown_target_data);
            
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            LDADA_Error(n_t)=LDADA_Error(n_t)+ (1/N_simulations)*Error;
        end
        
        if(GFK_active == 1)
            estimated_target_labels = GFKClassify(  known_source_data, known_source_labels, unknown_source_data,...
                known_target_data, known_target_labels, unknown_target_data);
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            GFK_Error(n_t) =  GFK_Error(n_t) + (1/N_simulations)*Error;
        end
        
        
        if(EA_active == 1)
            estimated_target_labels = EasyAdaptPlus(known_source_data, known_source_labels, known_target_data, known_target_labels, unknown_target_data, labels  );
            
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            EA_Error(n_t,1) = EA_Error(n_t,1) + (1/N_simulations)*Error;
        end
        
        if SVM_active == 1
            svmstr=svmTrainOneAll([known_source_data; known_target_data], [known_source_labels; known_target_labels], 0);
            estimated_target_labels=svmClassifyOneAll( svmstr,  unknown_target_data );
            
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            SVM_Error(n_t,1) =  SVM_Error(n_t,1) + (1/N_simulations)*Error;
        end
        
        if(SSL_active == 1)
            %N_NearestNeighbors = KNN_best_SSL;
            % Calculate Weight matrix regularly - K-NN
            W_all = adjacency_weighted([images_source;images_target], 2*N_NearestNeighbors, 0);
            
            % Calculate Weight matrix regularly - K-NN
            W_target = adjacency_weighted(images_target, N_NearestNeighbors, 0);
            
            % SSL Solution target only
            [ ssl1, ssl2] = SSL(W_target, N_target_members, N_classes, known_target_inds, known_target_labels, ...
                unknown_target_labels, N_known_target_labels, N_target_unlabeled_members);
            SSL_Error1_target(n_t,1) = SSL_Error1_target(n_t,1) + (1/N_simulations)*ssl1*100/N_target_unlabeled_members;
            SSL_Error2_target(n_t,1) = SSL_Error2_target(n_t,1) + (1/N_simulations)*ssl2*100/N_target_unlabeled_members;
            
            % SSL Solution: source + target
            [ ssl1, ssl2] = SSL(W_all, N_source_members+N_target_members, N_classes, ...
                [1:N_source_members N_source_members+known_target_inds], ...
                [f_source; known_target_labels], unknown_target_labels, N_known_target_labels + N_source_members, N_target_unlabeled_members);
            SSL_Error1_all(n_t,1) = SSL_Error1_all(n_t,1) + (1/N_simulations)*ssl1*100/N_target_unlabeled_members;
            SSL_Error2_all(n_t,1) = SSL_Error2_all(n_t,1) + (1/N_simulations)*ssl2*100/N_target_unlabeled_members;
        end
        
        
        if NN_active == 1
            known_labels = [known_source_labels; known_target_labels];
            labeled_data = [known_source_data; known_target_data];
            
            distance_mat=pdist2(labeled_data,unknown_target_data);
            [minvals, mininds]=min(distance_mat,[],1);
            estimated_target_labels=known_labels(mininds);
            Error =  100*sum(unknown_target_labels~=estimated_target_labels)/N_target_unlabeled_members;
            
            Err_NN(n_t,1)= Err_NN(n_t,1) + (1/N_simulations)*Error;
        end
        
    end
    
    
    if(DASGA_active)
        save Output_Data/DASGA_Error.mat DASGA_Error
    end
    
    
    if(SVM_active)
        save Output_Data/SVM_Error.mat SVM_Error
    end
    
    if(EA_active)
        save Output_Data/EA_Error.mat EA_Error
    end
    
    if(Dama_active)
        save Output_Data/Dama_Error.mat Dama_Error
    end
    
    if(SSL_active)
        save Output_Data/SSL_Error1_target.mat SSL_Error1_target
        save Output_Data/SSL_Error2_target.mat SSL_Error2_target
        save Output_Data/SSL_Error1_all.mat SSL_Error1_all
        save Output_Data/SSL_Error2_all.mat SSL_Error2_all
    end
    
    if(SA_active)
        save Output_Data/SA_Error.mat SA_Error
    end
    
    if(NN_active)
        save Output_Data/Err_NN.mat Err_NN
    end
    
    if(GFK_active)
        save Output_Data/GFK_Error.mat GFK_Error
    end
    
    if(SCA_active)
        save Output_Data/SCA_Error.mat SCA_Error
    end
    
    if(JGSA_active)
        save Output_Data/JGSA_Error.mat JGSA_Error
    end
    
    if(LDADA_active)
        save Output_Data/LDADA_Error.mat LDADA_Error
    end
    
end
