clear all;
close all;
clc;

subset_sizes = 6:6:36;

N_classes = 10;
N_simulations = 30;
N_train_vect = 6:2:60;
random_indices2 = zeros(N_simulations, max(N_train_vect));

for jj = 1:N_simulations
   random_indices(jj, :) = randperm(360, max(N_train_vect)); 
end

save('random_indices', 'random_indices')