function [ mutuals_s, mutuals_t ] = MatchLabelsCorrected( f_source, f_target, labeled_indices_target, labeled_indices_source )
    
    %Assume all source samples are labeled
   % labeled_indices_source=1:length(f_source);

    % Form reference indices to rectify eigenvectors
    class_names=sort(unique(f_source));
    N_classes=length(class_names);
    labeled_indices_source_rectif=zeros(size(labeled_indices_target));
    for iclass=1:N_classes
        class_name=class_names(iclass);
        num_to_add=sum(f_target(labeled_indices_target)==class_name);
        labeled_indices_source_to_add=find(f_source(labeled_indices_source)==class_name)';     
        labeled_indices_source_to_add=labeled_indices_source_to_add(randperm(length(labeled_indices_source_to_add), num_to_add));
        labeled_indices_source_rectif(find(f_target(labeled_indices_target)==class_name))=labeled_indices_source_to_add; 
    end
    
    mutuals_s=labeled_indices_source_rectif;
    mutuals_t=labeled_indices_target;







end
