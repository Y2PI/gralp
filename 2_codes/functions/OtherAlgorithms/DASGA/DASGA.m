% % This code is based on the algorithm proposed in our work; Domain 
% % Adaptation on Graphs by Learning Aligned Graph Bases
% % Authors: Elif Vural (velif@metu.edu.tr)
% %          Mehmet Pilanc� (mehmet.pilanci@metu.edu.tr)
% 
function [  f_target_est ] = DASGA( params, isOnehot)
W = params.W;
W_target = params.W_target;
f_source = params.f_source;
f_target = params.f_target;
mu = params.mu; % optimization parameter
N_eig = params.N_eigenvectors;
N_iterations = params.N_iterations;
mu_2 = params.mu_2;
labeled_indices_target = params.labeled_target_indices;
labeled_indices_source = params.labeled_source_indices;

labels = params.labels;

N_train = length(labeled_indices_target);
N_source=length(labeled_indices_source);
N_classes = length(labels);

N_source_member = length(f_source);
N_target_member = length(f_target);
N_target_unlabeled_member = N_source_member-N_train;

alpha_t_all = cell(N_classes,1);
alpha_s_all = cell(N_classes,1);
T_all = cell(N_classes,1);
% cost = zeros(N_iterations+1, 1);

% %Error_indiv = zeros(N_classes, length(N_iterations)+1);
% weight_scale = 16;
% Weights_T = 200*ones(N_eig, N_eig);
% for ii = 1:N_eig
%     for jj = 1:N_eig
%         %if(jj == 1 || ii == 1 || ii == 2 || jj == 2 || ii == 3 || jj == 3)
%         if(jj == 1 || ii == 1 || ii == 2 || jj == 2 || ii == 3 || jj == 3 )
%             % || ii == 4 || jj == 4 || ii == 5 || jj == 5 || ii == 6 || jj == 6
%             Weights_T(ii, jj) = exp(abs(ii-jj)^2/weight_scale);
%         end
%     end
% end




weight_scale = 8;
Weights_T = 200*ones(N_eig, N_eig);
for ii = 1:N_eig
    for jj = 1:N_eig
        Weights_T(ii, jj) = exp(abs(ii-jj)^2/weight_scale);
    end
end

%Weights_T=full(adjacency_weighted(dist, N_eig-1)); %round(N_eig/2)));



% Laplacian calculations
D = diag(sum(W, 2));
L = D - W;
[U_s, Eig_s] = eig(L);
Eig_s = sum(Eig_s, 2);
[~, index]= sort(Eig_s, 'ascend');
U_s = U_s(:, index); %Sort eigenvectors in ascending order
U_s = U_s(:, 1:N_eig);



D_target = diag(sum(W_target, 2));
L_target = D_target - W_target; % Laplacian
[U_t, Eig_t] = eig(L_target);
Eig_t = sum(Eig_t, 2);
[~, index]= sort(Eig_t, 'ascend');
U_t = U_t(:, index); %Sort eigenvectors in ascending order
U_t = U_t(:, 1:N_eig);

Error = zeros(N_iterations + 1, 1);
U_s_orig = U_s;
U_t_orig = U_t;


if( ~isfield(params,"unlabeled_indices_target") )
    unlabeled_indices_target=setdiff(1:N_target_member, labeled_indices_target);
else
    unlabeled_indices_target = params.unlabeled_indices_target;
end
%unlabeled_indices_source = setdiff(1:N_source_member, labeled_indices_source);
%unlabeled_indices_target = [unlabeled_indices_source setdiff(1:N_target_member, labeled_indices_target)];

% Selection matrix for target domain
S_t = full(sparse(1:N_train, labeled_indices_target, ones(N_train,1), N_train, N_target_member));
% Selection matrix for source domain
S_s =  full(sparse(1:N_source, labeled_indices_source, ones(N_source,1), N_source, N_source_member));






if(isOnehot)
    
    [ mutuals_s, mutuals_t ] = MatchLabelsCorrected( f_source, f_target, labeled_indices_target,labeled_indices_source );  
    [U_t_rectified] = RectifyEigenvectors( U_s, U_t, mutuals_s, mutuals_t );
    
    
    temp_labels = zeros(N_target_member, N_classes);
    for ii = 1:N_classes
        f_s = f_source;
        f_t = f_target;
        
        inds1 = f_s == labels(ii);
        inds2 = ~inds1;
        f_s(inds1) = -1;
        f_s(inds2) = 1;
        
        inds1 = f_t == labels(ii);
        inds2 = ~inds1;
        f_t(inds1) = -1;
        f_t(inds2) = 1;        
        
   
        inds_source = labeled_indices_source;
        U_s = U_s_orig; 
        U_t = U_t_orig;
        
     %   [U_t_rectified] = RectifyEigenvectors( U_s, U_t, labeled_indices_target, labeled_indices_target);
        

      
      
        y_t = f_t(labeled_indices_target);
        y_s = f_s(inds_source);
        A_s = U_s'*(S_s'*S_s)*U_s;
        A_t = U_t_rectified'*(S_t'*S_t)*U_t_rectified;
        B_s = U_s'*S_s';
        B_t = U_t_rectified'*S_t';
        
        
        aa = (1/mu)*(A_t + mu*eye(size(A_t)))*(A_s + mu*eye(size(A_s))) - mu*eye(size(A_t));
        bb = (1/mu)*(A_t + mu*eye(size(A_t)))*B_s*y_s + B_t*y_t;
        
        alpha_s = aa\bb;
        clear aa bb;
        alpha_t = (1/mu)*((A_s + mu*eye(size(A_s)))*alpha_s - B_s*y_s);
        alpha_t_all{ii, 1} = alpha_t;
        alpha_s_all{ii, 1} = alpha_s;
        base_output = transpose(alpha_t'*U_t_rectified');
        temp_labels(:, ii) = base_output;
        T_all{ii, 1} = U_t\U_t_rectified;
    end
    % Assign labels to all members in target and source domain
    [~, f_target_est] = min(temp_labels, [], 2);
    
    Error(1) = length(find(f_target_est(unlabeled_indices_target)-f_target(unlabeled_indices_target)))*100/N_target_unlabeled_member;
    
    %%%%%%%%%%%%%%%
    temp_labels = zeros(N_target_member, N_classes);
    for  n_it = 1:N_iterations
        for ii = 1:N_classes
            f_s = f_source;
            f_t = f_target;
            
            inds1 = f_s == labels(ii);
            inds2 = ~inds1;
            f_s(inds1) = -1;
            f_s(inds2) = 1;
            
            inds1 = f_t == labels(ii);
            inds2 = ~inds1;
            f_t(inds1) = -1;
            f_t(inds2) = 1;
            
            y_t = f_t(labeled_indices_target);
            y_s = f_s(inds_source);
            T = T_all{ii, 1};
            U_t = U_t_orig; 
            U_s = U_s_orig;
            alpha_s = alpha_s_all{ii, 1};
            alpha_t = alpha_t_all{ii, 1};
            
            % optimize T, fixing alpha_s and alpha_t
            F = y_t * alpha_t';
            D = S_t*U_t;
            A1 = FindMatrixA1(alpha_t, S_t, U_t);
            A2 = FindMatrixA2( Weights_T );
            A = sparse(A1 + mu_2*A2);
   
            
             t0 = T(:);            % Starting guess
%             options = optimoptions(@fmincon,'Algorithm','sqp');
%             options = optimoptions('fmincon','GradConstr','on');
%             options = optimoptions('fmincon','GradObj','on');
            options = optimoptions('fmincon','Algorithm','sqp','GradConstr','on','GradObj','on');
            
            %'MaxFunctionEvaluations', 20000, 'MaxIterations',10000);

            %     options.MaxFunEvals = 5000;
            %lb = [ ]; ub = [ ];   % No upper or lower bounds
            lb=-1*ones(size(t0));
            ub=1*ones(size(t0));
            [t,tval] = fmincon(@(t)objfungrad(t, A, D, F, Weights_T, alpha_t, y_t, N_eig, mu_2),t0,[],[],[],[],lb,ub,...
                @(t)confungrad(t, N_eig),options);
        
            T = reshape(t, size(U_t,2), size(U_t,2));
            clear A1 A2;
            
            % optimize alpha_s and alpha_t, fixing T
            U_t_mod = U_t*T;
            A_t = U_t_mod'*(S_t'*S_t)*U_t_mod;
            A_s = U_s'*(S_s'*S_s)*U_s;
            B_t = U_t_mod'*S_t';
            B_s = U_s'*S_s';
            
            aa = (1/mu)*(A_t + mu*eye(size(A_t)))*(A_s + mu*eye(size(A_s))) - mu*eye(size(A_t));
            bb = (1/mu)*(A_t + mu*eye(size(A_t)))*B_s*y_s + B_t*y_t;
            alpha_s = aa\bb;
            clear aa bb;
            alpha_t = (1/mu)*((A_s + mu*eye(size(A_s)))*alpha_s - B_s*y_s);
            
            T_all{ii, 1} = T;
            alpha_t_all{ii, 1} = alpha_t;
            alpha_s_all{ii, 1} = alpha_s;
            
            base_output = transpose(alpha_t'*U_t_mod');
            temp_labels(:, ii) = base_output;
            %Error_indiv(ii, n_it+2) = Error_indiv(n_it+2) + 100*sum(f_target(unlabeled_indices_target)~=soln(unlabeled_indices_target))/length(unlabeled_indices_target)/N_simulations;
            %cost(ii, n_it+1) = norm(S_s * U_s * alpha_s - y_s)^2 + norm(S_t * U_t_mod * alpha_t - y_t)^2 + mu*norm(alpha_s - alpha_t)^2 + mu_2*norm(Weights_T.*T,'fro')^2;
        end
        % Assign labels to all members in target and source domain
        [~, f_target_est] = min(temp_labels, [], 2);
        
        Error(n_it+1, 1) = length(find(f_target_est(unlabeled_indices_target)-f_target(unlabeled_indices_target)))*100/N_target_unlabeled_member;
    end
    DASGA_Error= Error(end, 1);
else
    U_s = U_s_orig; 
    U_t = U_t_orig;


    [ mutuals_s, mutuals_t ] = MatchLabelsCorrected( f_source, f_target, labeled_indices_target, labeled_indices_source );
    [U_t_rectified] = RectifyEigenvectors( U_s, U_t, mutuals_s, mutuals_t );

    
    y_t = f_target(labeled_indices_target);
    y_s = f_source(labeled_indices_source);
    A_s = U_s'*(S_s'*S_s)*U_s;
    A_t = U_t_rectified'*(S_t'*S_t)*U_t_rectified;
    B_s = U_s'*S_s';
    B_t = U_t_rectified'*S_t';
    
    aa = (1/mu)*(A_t + mu*eye(size(A_t)))*(A_s + mu*eye(size(A_s))) - mu*eye(size(A_t));
    bb = (1/mu)*(A_t + mu*eye(size(A_t)))*B_s*y_s + B_t*y_t;
    
    alpha_s = aa\bb;
    clear aa bb;
    alpha_t = (1/mu)*((A_s + mu*eye(size(A_s)))*alpha_s - B_s*y_s);
    
    base_output = transpose(alpha_t'*U_t_rectified');
    base_solution = AssignLabels( base_output, labels );
    Error(1) = Error(1) + 100*sum(base_solution(unlabeled_indices_target)~=f_target(unlabeled_indices_target))/length(unlabeled_indices_target);
    
    T = U_t\U_t_rectified;
%     cost(1) = norm(S_s * U_s * alpha_s - y_s)^2 + norm(S_t * U_t_rectified * alpha_t - y_t)^2 + mu*norm(alpha_s - alpha_t)^2 + mu_2*norm(Weights_T.*T,'fro')^2;
    
    for  n_it = 1:N_iterations
        % optimize T, fixing alpha_s and alpha_t
        F = y_t * alpha_t';
        D = S_t*U_t;
        A1 = FindMatrixA1(alpha_t, S_t, U_t);
        A2 = FindMatrixA2( Weights_T );
        A = sparse(A1 + mu_2*A2);
         
        
        t0 = T(:);            % Starting guess
        %             options = optimoptions(@fmincon,'Algorithm','sqp');
        %             options = optimoptions('fmincon','GradConstr','on');
        %             options = optimoptions('fmincon','GradObj','on');
        options = optimoptions('fmincon','Algorithm','sqp','GradConstr','on','GradObj','on');
        
        %     options.MaxFunEvals = 5000;
        lb = [ ]; ub = [ ];   % No upper or lower bounds
        [t,tval] = fmincon(@(t)objfungrad(t, A, D, F, Weights_T, alpha_t, y_t, N_eig, mu_2),t0,[],[],[],[],lb,ub,...
            @(t)confungrad(t, N_eig),options);

        
        T = reshape(t, size(U_t,2), size(U_t,2));
        clear A1 A2;
        
        % optimize alpha_s and alpha_t, fixing T
        U_t_mod = U_t*T;
        A_t = U_t_mod'*(S_t'*S_t)*U_t_mod;
        A_s = U_s'*(S_s'*S_s)*U_s;
        B_t = U_t_mod'*S_t';
        B_s = U_s'*S_s';
        
        aa = (1/mu)*(A_t + mu*eye(size(A_t)))*(A_s + mu*eye(size(A_s))) - mu*eye(size(A_t));
        bb = (1/mu)*(A_t + mu*eye(size(A_t)))*B_s*y_s + B_t*y_t;
        alpha_s = aa\bb;
        clear aa bb;
        alpha_t = (1/mu)*((A_s + mu*eye(size(A_s)))*alpha_s - B_s*y_s);
        
%         cost(n_it+1) = norm(S_s * U_s * alpha_s - y_s)^2 + norm(S_t * U_t_mod * alpha_t - y_t)^2 + mu*norm(alpha_s - alpha_t)^2 + mu_2*norm(Weights_T.*T,'fro')^2;
        output2 = transpose(alpha_t'*U_t_mod');
        f_target_est= AssignLabels( output2, labels );
        Error(n_it+1,1) =  100*sum(f_target(unlabeled_indices_target)~=f_target_est(unlabeled_indices_target))/length(unlabeled_indices_target);
    end
    DASGA_Error = Error(end, 1);
end
f_target_est=f_target_est(unlabeled_indices_target);
end