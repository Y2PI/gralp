function [ U_new ] = RectifyEigenvectors( V, U, mutuals_s, mutuals_t  )
    V_sub = V(mutuals_s,:);
    U_sub = U(mutuals_t,:);
    U_new = zeros(size(U));
    for ii=1:size(U,2)
        u_i_sub = U_sub(:,ii);
        
        innerProducts_sub = u_i_sub'*V_sub;
        [~, ind_sub] = max(abs(innerProducts_sub));
        U_new(:,ii) = sign(innerProducts_sub(ind_sub))*U(:,ii);
    end
end


