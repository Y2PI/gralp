function [ A2 ] = FindMatrixA2( W )
% V = W'*W;
% n = size(W, 2);
% n2 = n^2;
% A2 = sparse(n2, n2);
% 
% for r = 1:n2
%     j = ceil(r/n);
%     i = mod(r-1, n) + 1;
%     for k = 1:n
%         A2(r, (k-1)*n+i) = V(k,j);
%     end
% end

V = W(:);
n = length(V);

% A2 = diag(V.^2);
A2 = sparse(n, n);
for r = 1:n
    A2(r, r) = V(r)^2;
end
end

