function [ output ] = AssignLabels( input, labelset )
N = length(input);
output = zeros(size(input));
for ii=1:N
    [~, ind] = min(abs(labelset - input(ii)));
    output(ii) = labelset(ind);
end
end

