% function image_selection_indices = RandomIndexSelection(N_classes, classSize, subsetSize)
% image_selection_indices = zeros(1,subsetSize*N_classes);
% for nn = 1:N_classes
%     image_selection_indices((nn-1)*subsetSize+1: nn*subsetSize) = classSize*(nn-1)+randperm(classSize, subsetSize);
% end
% end

function image_selection_indices = RandomIndexSelection(N_classes, classSize, subsetSize)
image_selection_indices = zeros(1,subsetSize*N_classes);
inds = randperm(classSize, subsetSize);
for nn = 1:N_classes
    image_selection_indices((nn-1)*subsetSize+1: nn*subsetSize) = classSize*(nn-1)+inds;
end
end