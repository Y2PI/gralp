function [c,ceq,DC,DCeq] = confungrad(x, N_eigenvectors)
c=[];
x_mat = reshape(x, N_eigenvectors, N_eigenvectors);
ceq= transpose(sum(x_mat.^2,1)-1);
% Gradient of the constraints:
if nargout > 2
    DC= [];
    DCeq = zeros(N_eigenvectors, N_eigenvectors);
    for ii=1:N_eigenvectors
        DCeq((ii-1)*N_eigenvectors+1:ii*N_eigenvectors, ii) = 2*x((ii-1)*N_eigenvectors+1:ii*N_eigenvectors);
    end
end