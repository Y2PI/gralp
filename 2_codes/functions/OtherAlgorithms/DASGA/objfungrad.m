function [f,gradf] = objfungrad(t, A, D, F, Weights_T, alpha_t, y_t, N_eigenvectors, mu_2)
t_mat = reshape(t, N_eigenvectors, N_eigenvectors);
f = norm(D*t_mat*alpha_t-y_t)^2 + mu_2*norm(Weights_T.*t_mat,'fro')^2;
% Gradient of the objective function:
if nargout  > 1
    gradf = 2*(A*t - reshape(D'*F, [], 1));
end