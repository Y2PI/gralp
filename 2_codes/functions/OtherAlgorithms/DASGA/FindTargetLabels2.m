function [ output ] = FindTargetLabels2(y_s, y_t, S_s, S_t, U_s, U_t, mu )

A_s = U_s'*(S_s'*S_s)*U_s;
A_t = U_t'*(S_t'*S_t)*U_t;
B_s = U_s'*S_s';
B_t = U_t'*S_t';

a = (1/mu)*(A_t + mu*eye(size(A_t)))*(A_s + mu*eye(size(A_s))) - mu*eye(size(A_t));
b = (1/mu)*(A_t + mu*eye(size(A_t)))*B_s*y_s + B_t*y_t;

alpha_s = a\b;
alpha_t = (1/mu)*((A_s + mu*eye(size(A_s)))*alpha_s - B_s*y_s);

% f_target_hat = (V'*(M'*M)*V*lambda)\(f_hat+V'*M'*labels*lambda);
output = transpose(alpha_t'*U_t');

end
