%% Weight Parameter Choice
d = 0:0.001:0.3075;
w = exp(-d/(2*0.01));
figure
plot(d,w)
hold on
w = exp(-d/(2*0.02));
plot(d,w, 'c')
w= exp(-d/(2*0.03));
plot(d,w,'r')
w= exp(-d/(2*0.04));
plot(d,w,'g')
legend('var = 0.01','var = 0.02', 'var = 0.03', 'var = 0.04');
xlabel('Distance');
ylabel('Weight');