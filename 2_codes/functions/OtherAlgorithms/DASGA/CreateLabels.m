function [ labels ] = CreateLabels(distance, N_class, class_size)
% Finding distances between classes: Find distance of closest
% element in class B for each element in class A. Then sum all
% these distances
class_distances = zeros(N_class, N_class);
for m=1:N_class
    for n=(m+1):N_class
        d = distance(class_size*(n-1)+1:n*class_size, class_size*(m-1)+1:m*class_size);
        class_distances(n, m) = sum(min(d));
        %classDistances(jj, ii) = sum(sum(d));
        class_distances(m, n) = class_distances(n, m);
    end
end

labels = mdscale(class_distances, 1, 'Criterion', 'metricstress');
end

