function [ error, resulting_labels ] = NearestNeighborSolution( distance_mat, known_labels, true_labels )
[~, inds] = min(distance_mat);
resulting_labels = known_labels(inds, 1);
error = length(find(resulting_labels-true_labels));

% function [ error, resulting_labels ] = NearestNeighborSolution( labeled_images, known_labels, unlabeled_images, true_labels )
% resulting_labels = zeros(size(unlabeled_images, 2), 1);
% for m=1:size(unlabeled_images, 2)
%     [~, ind] = min(sqrt(sum((labeled_images - repmat(unlabeled_images(:, m), 1, size(labeled_images, 2))).^2, 1)), [], 2);
%     resulting_labels(m) = known_labels(ind,1);
% end
% error = length(find(resulting_labels-true_labels));

end

