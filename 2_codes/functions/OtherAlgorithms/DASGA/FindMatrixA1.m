function [ A ] = FindMatrixA1(alpha_t, S_t, U_t )
E = alpha_t*alpha_t';
D = S_t*U_t;
B = D'*D;
n = size(U_t, 2);
n2 = n^2;
A = sparse(n2, n2);

% for r = 1:n2
%     j = ceil(r/n);
%     i = mod(r-1, n) + 1;
%     for k = 1:n
%         for m = 1:n
%             %A(r, (m-1)*n+k) = E(i, k) * B(m, j);
%             A(r, (m-1)*n+k) = E(i, m) * B(k, j);
%         end
%     end
% end


for r = 1:n2
    j = ceil(r/n);
    i = mod(r-1, n) + 1;
    for k = 1:n
        for m = 1:n
            A(r, (m-1)*n+k) = B(i, k) * E(j, m);
        end
    end
end

end

