function image_selection_indices = RandomIndexSelection2(N_classes, classSize, subsetSize)
image_selection_indices = zeros(1,subsetSize*N_classes);
for nn = 1:N_classes
    image_selection_indices(1,(nn-1)*subsetSize+1: nn*subsetSize) = classSize*(nn-1)+randperm(classSize, subsetSize);
end
end