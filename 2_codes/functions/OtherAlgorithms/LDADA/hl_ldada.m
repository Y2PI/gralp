function [Zs, Zt] = hl_ldada(S, T, maxiter,predictor)
%HL_LDADA implements LDA-inspired Domain Adaptation (LDADA) appearing in
% An Embarassingly Simple Approach to Visual Domain Adaptation
% IEEE Transactions on Image Processing, 2018
% Hao Lu, Chunhua Shen, Zhiguo Cao, Yang Xiao, Anton van den Hengel
%

ldada.maxiter = maxiter; 
ldada.predictor = predictor; 

Xs = S.Xs;
Xt = T.Xt;
Ys = S.Ys;
Xtl = T.Xtl;
Ytl = T.Ytl;

Xt_all = [Xt Xtl];
% mean subtraction
meanXs = mean(Xs, 2);
meanXt_all = mean(Xt_all, 2);
Xs = bsxfun(@minus, Xs, meanXs);
Xt_all = bsxfun(@minus, Xt_all, meanXt_all);

% L2 normalization
Xs = bsxfun(@times, Xs, 1./max(1e-12, sqrt(sum(Xs.^2))));
Xt_all = bsxfun(@times, Xt_all, 1./max(1e-12, sqrt(sum(Xt_all.^2))));

Xt = Xt_all(:,1:size(Xt,2));




Xtl = Xt_all(:,(size(Xt,2)+1):end);


% pseudo set setting
P.Xt = []; 
P.Yt = [];
P.id = [];
P.predictor = ldada.predictor;
opt.nclasses = max(unique(Ys)); % Assumes class labels start from 1 and number of classes is equal to maximum of label function
P.nclasses = length(unique(Ys));

% init W
W = eye(size(Xs, 1));
Wp = ones(size(Xs, 1), opt.nclasses);

accp = 101;
for t = 1:ldada.maxiter
 if t > 1, Wp = W; accp = P.acc_cv; end
  % update Yt given W
  P = labeling_target(Xs, Xtl, Ys,Ytl,Xt, W, P);
  % update W given Yt
  W = learning_csproj(Xs, P.Xt, Ys, P.Yt);

  deltaW = norm(W - Wp, 'fro') / norm(Wp, 'fro');
  deltaAcc = abs(P.acc_cv - accp) / accp;

  if deltaW < 1e-3 || deltaAcc < 1e-3
      break; 
  end
end

Zs = W' * Xs;
Zt = W' * Xt;

end

function W = learning_csproj(Xs, Xt, Ys, Yt)

[d, ~] = size(Xt);

if size(Ys, 2) ~= 1, Cs = find(sum(Ys) ~= 0); else, Cs = unique(Ys)'; end
if size(Yt, 2) ~= 1, Ct = find(sum(Yt) ~= 0); else, Ct = unique(Yt)'; end
if numel(Cs) < numel(Ct), C = Cs; else, C = Ct; end

if size(Ys, 2) == 1, idxsi = bsxfun(@eq, Ys, C); else, idxsi = Ys; end
if size(Yt, 2) == 1, idxti = bsxfun(@eq, Yt, C); else, idxti = Yt; end

Ms = bsxfun(@rdivide, Xs * idxsi, sum(idxsi)+1e-10);
Mt = bsxfun(@rdivide, Xt * idxti, sum(idxti)+1e-10);
Ms_ = bsxfun(@rdivide, Xs * ~idxsi, sum(~idxsi)+1e-10);
Mt_ = bsxfun(@rdivide, Xt * ~idxti, sum(~idxti)+1e-10);

Sb = (Ms + Mt - Ms_ - Mt_) / 2;
v = sum(Ms .* Sb);
u = sum(Ms.^2) .* sum(Mt.^2);

W = zeros(d, max(numel(Cs), numel(Ct)));
W(:, C) = bsxfun(@times, v ./ u, Mt);

end

function P = labeling_target(Xs, Xtl, Ys, Ytl, Xu, W, P)
TrainData = [Xs Xtl];
labels  = [Ys;Ytl];
Xt = [Xtl Xu];
numLabeledTarget = length(Ytl);
model = train(labels, sparse(double(TrainData'*W)), '-s 5 -c 1 -B 1 -q');
score = model.w * [W'*Xt; ones(1, size(Xt, 2))];

th = mean(score, 2)';
[confidence, C] = max(score, [], 1);
idxpos = confidence > th(C);

P.id = idxpos;
idxpos(1:numLabeledTarget) = true;
P.Xt = Xt(:, idxpos);
P.Yt = C(idxpos)';
P.Yt(1:numLabeledTarget) = Ytl;
% circular validation
P.acc_cv = 0;
Wt = learning_csproj(P.Xt, Xs, P.Yt, Ys);
predictedLabels = learn_predict_labels(Wt'*P.Xt, Wt'*Xs,...
    P.Yt, Ys, P);

P.acc_cv = normAcc(Ys,predictedLabels );

end

function C = learn_predict_labels(Xs, Xt, Ys, Yt, opt)

switch opt.predictor
  case 'ldada'
    [~, C] = max(Xt, [], 1); C = C';
  case 'svm'
    C = learnPredictSVM(Xs, Xt, Ys, Yt);
  otherwise
    error('unsupported predictor option')
end

end
