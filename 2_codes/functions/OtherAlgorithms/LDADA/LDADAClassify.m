function [ labels_target_unlabeled ] = LDADAClassify(  data_source_labeled, labels_source, ...
    data_target_labeled, labels_target, data_target_unlabeled,maxiter,predictor)
%
% An Embarassingly Simple Approach to Visual Domain Adaptation
% IEEE Transactions on Image Processing, 2018
% Hao Lu, Chunhua Shen, Zhiguo Cao, Yang Xiao, Anton van den Hengel


% INPUTS:
% data_source_labeled   : Labeled source data, each row is a data sample
% labels_source         : Labels of data_source_labeled, each row is a label
% data_target_labeled   : Labeled target data, each row is a data sample
% labels_target         : Labels of data_target_labeled, each row is a label
% data_target_unlabeled : Unlabeled target data, each row is a data sample
% maxiter               : Maximum iterations default: 10
% predictor             : ldada or svm, default: ldada
%
% OUTPUTS:
% labels_target_unlabeled: Estimated labels of unlabeled target data

if nargin < 6
    maxiter = 10;
    predictor = 'ldada';
end
if nargin < 7
    predictor = 'ldada';
end
    
S.Xs = data_source_labeled';
S.Ys = labels_source;
T.Xt = data_target_unlabeled';
T.Xtl = data_target_labeled';
T.Ytl = labels_target;
numLabeledTarget = length(T.Ytl);
[~,Zt] = hl_ldada(S, T, maxiter,predictor);
[~, labels_target_unlabeled] = max(Zt, [], 1);
labels_target_unlabeled = labels_target_unlabeled';
end