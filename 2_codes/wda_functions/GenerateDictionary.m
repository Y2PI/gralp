% This function takes Laplacian and 
% designed functions of the graph
% And returns the wavelet dictonary

function [ dict ] = GenerateDictionary( N,L,c,arange )
    dict = zeros(size(L,1),size(L,1)*5);
    for i = 1 :N: N*size(L,1)
        delta = zeros(size(L,1),1);
        delta(ceil(i/N)) = 1;
        wpall = sgwt_cheby_op(delta,L,c,arange);
        for j = 1: N
            dict(:,i+(j-1)) = wpall{j};
        end
    end
end

