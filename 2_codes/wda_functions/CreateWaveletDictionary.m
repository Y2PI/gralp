function [wavelet_dictionary]= CreateWaveletDictionary(W,N)
% Calculating laplacian of adjancency matrix
D=diag(sum(W));%unnormalized laplacian

L = (D^(-1/2))*(D-W)*(D^(-1/2));
% Wavelet Transform calculated here

    %% Design filters for transform
    lmax=sgwt_rough_lmax(L);

%     [g,gp,t]=sgwt_filter_design(lmax,N-1);
      [g,t]=sgwt_filter_design(lmax,N-1);

    arange=[0 lmax];


    %% Chebyshev polynomial approximation
    m=50; % order of polynomial approximation
    for k=1:numel(g)
      c{k}=sgwt_cheby_coeff(g{k},m,m+1,arange);
    end


    %% Dictionaries are created for both source and target by giving deltas to each node
    wavelet_dictionary = GenerateDictionary( N,L,c,arange );
%     D=diag(sum(W));
%     %unnormalized laplacian
%     L=D-W;
%     %Normalized Laplacian
%     Lnor=D^(-1/2)*L*D^(-1/2);
%     W_dict=[];
%     N=size(L,1);
%     
%     if(nargin<2)
%         node_ids=1:N;
%     end
%     
%     for jcenter=node_ids
%         d=sgwt_delta(N,jcenter);
%         wavelet=wavelettransform(L,d);
%        % W_dict=[W_dict wavelet];
%         W_dict=[W_dict wavelet(1:5)];
%     end
%     wavelet_dictionary = (cell2mat(W_dict));
end