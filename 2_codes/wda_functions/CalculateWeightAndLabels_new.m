function [ W,f,X] = CalculateWeightAndLabels_new(data,sigma,k)

for i = 1 : length(data)
    imnum(i) = size(data{i},2);
end
label_ptrs = cumsum(imnum);
f(1:label_ptrs(1)) = 1;
for i = 2 : length(label_ptrs)
    f(label_ptrs(i-1)+1:label_ptrs(i)) = i;
end

%% Calculate Euclidean distances
X = cell2mat(data');
costs = dist(X);
%% Calculate Weights
W = zeros(size(costs));                            % Initialization of Weight matrix
for i = 1 : size(costs,1)
    [sorted idx] = sort(costs(i,:),'ascend');
    for j = 1 : k%length(idx)
       if(i ~= idx(j))
            W(i,idx(j)) = exp(-((costs(i,idx(j))/sigma)^2)/2); % Gaussian Weights
        end
    end
end
W = (W + W')/2;

end
