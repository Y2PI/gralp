function [ W,f,X] = CalculateWeightAndLabels(data,sigma,k)

for i = 1 : length(data)
    imnum(i) = size(data{i},2);
end
%% Calculate Euclidean distances
numImageinOneClass_sum = 0;
for i = 0 : size(data,1)-1
    numImageinOneClass = size(data{i+1},2);
    numImageinOneClass_sum =numImageinOneClass_sum+ size(data{i+1},2);
    X(:, 1+numImageinOneClass_sum - numImageinOneClass: numImageinOneClass_sum) = (data{i+1});
end
costs = dist(X);
%% Calculate Weights
W = zeros(size(costs));                            % Initialization of Weight matrix
for i = 1 : size(costs,1)
    [sorted idx] = sort(costs(i,:),'ascend');
    for j = 1 : k%length(idx)
       if(i ~= idx(j))
            W(i,idx(j)) = exp(-((costs(i,idx(j))/sigma)^2)/2); % Gaussian Weights
        end
    end
end
W = (W + W')/2;
%% Calculate Labels
for i = 1:10
    f((imnum(i)*(i-1))+1:imnum(i)*i) = i;
end
end
