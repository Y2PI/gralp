function [ Reconstructed_Label ] = Gralp( numofNodeSource,numofNodeTarget,mu,gama_s,gama_t,dict,W1,W2,KnownLabel_source,KnownLabel_target,source_known_nodes,target_known_nodes )
    % All source and target node list
    snodes = 1:numofNodeSource;
    tnodes = 1:numofNodeTarget;
    % Calculate the diagonal matrix
    D1=diag(sum(W1));
    D2=diag(sum(W2));
    % Create Selection matrices
    S_s = [];
    for i = 1: length((source_known_nodes))
        dummy = zeros (1,length(W1));
        tempidx = find( snodes == source_known_nodes(i));
        dummy(tempidx) = 1;
        S_s = [S_s ;dummy];
    end
    if(isempty(S_s))
        S_s = 0;
    end
    S_t  = [];
    for i = 1: length((target_known_nodes))
        dummy = zeros (1,length(W2));
        tempidx = find( tnodes == target_known_nodes(i));
        dummy(tempidx) = 1;
        S_t  = [S_t  ;dummy];
    end
    if(isempty(S_t))
        S_t = 0;
    end
    if(isempty(KnownLabel_target))
        KnownLabel_target = 0;
    end

%     D1_pow_0p5 = inv(sqrt(D1));
%     D2_pow_0p5 = inv(sqrt(D2));
%     
%     R_s = gama_s*(D1_pow_0p5)*(D1-W1)*(D1_pow_0p5);
%     R_t = gama_t*(D2_pow_0p5)*(D2-W2)*(D2_pow_0p5);

    R_s = gama_s*(D1-W1);
    R_t = gama_t*(D2-W2);

    A = S_t'*S_t + mu*dict{2}*dict{2}' ... 
                  - mu^2*dict{2}*dict{1}' ...
                  *pinv( S_s'*S_s +mu*dict{1}*dict{1}' + R_s)...
                  *dict{1}*dict{2}' + R_t;

    b = S_t'*KnownLabel_target + mu*dict{2}*(dict{1}')...
        *pinv((S_s')*S_s + mu*dict{1}*dict{1}'+R_s)*S_s'...
        *KnownLabel_source;

    Reconstructed_Label = pinv(A)*b;
end

