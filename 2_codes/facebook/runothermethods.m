%%  SVM: Source+ Target %%%%
train_data=[source_data(source_known_nodes,:); target_data(target_known_nodes,:)];
train_labels=[f_source(source_known_nodes) f_target(target_known_nodes)]';


if(length(unique(train_labels)) > 1)
    svm_str = svmTrainOneAll(train_data, train_labels);
    test_data = target_data(target_unknown_nodes,:);
    est_target = svmClassifyOneAll(svm_str, test_data)';
elseif(length(unique(train_labels)) == 1)
    est_target = repmat(unique(train_labels),length(target_unknown_nodes),1);
end
grn_target = f_target(target_unknown_nodes)';

errors_svm_st(iparam,itrial).conf = confusionmat(grn_target,est_target);
assert(isequal(size(errors_svm_st(iparam,itrial).conf), [2,2]))
%% NN: Source+Target  %%%%%%
train_data=[source_data(source_known_nodes,:); target_data(target_known_nodes,:)];
train_labels = [f_source(source_known_nodes) f_target(target_known_nodes)]';
test_data=target_data(target_unknown_nodes,:);

est_target=zeros(length(target_unknown_nodes),1);
for itest=1:size(test_data,1)
    test_image=test_data(itest,:);
    d_vect=sum((train_data-repmat(test_image,size(train_data,1),1)).^2,2);
    [~, minind] =min(d_vect);
    est_target(itest)=train_labels(minind);
end
grn_target = f_target(target_unknown_nodes)';
errors_nn_st(iparam,itrial).conf = confusionmat(grn_target,est_target);


%% SA Classifier %%%%

source_unknown_nodes = setdiff(1:size(source_data,1),source_known_nodes);

est_target = zeros(length(target_unknown_nodes),1);
est_target = SAClassify(  source_data(source_known_nodes,:),...
    f_source(source_known_nodes)', source_data(source_unknown_nodes,:),...
    target_data(target_known_nodes,:),  f_target(target_known_nodes)', ...
    target_data(target_unknown_nodes,:), 1, 'nn');

grn_target = f_target(target_unknown_nodes)';
errors_sa(iparam,itrial).conf = confusionmat(grn_target,est_target);



%% EasyAdaptPlus
est_target = zeros(length(target_unknown_nodes),1);
est_target = EasyAdaptPlus(source_data(source_known_nodes,:),f_source(source_known_nodes)', ...
    target_data(target_known_nodes,:), ...
    f_target(target_known_nodes)', target_data(target_unknown_nodes,:), ...
    1:2);
grn_target = f_target(target_unknown_nodes)';
errors_ea(iparam,itrial).conf = confusionmat(grn_target,est_target);


%% GFK
est_target = zeros(length(target_unknown_nodes),1);
est_target = GFKClassify(  source_data(source_known_nodes,:),...
    f_source(source_known_nodes)', source_data(source_unknown_nodes,:),...
    target_data(target_known_nodes,:), f_target(target_known_nodes)', ...
    target_data(target_unknown_nodes,:));
grn_target = f_target(target_unknown_nodes)';
errors_gfk(iparam,itrial).conf = confusionmat(grn_target,est_target);
%% Semi Supervised Learning-SSL
if ~isempty(target_known_nodes)
    [fu,fu_CMN]=SSL_facebook(W2,target_known_nodes_ssl,target_unknown_nodes_ssl,f_target(target_known_nodes_ssl));

    [fu_hard_vals, est_target]=max(fu,[],2);
    grn_target = f_target(target_unknown_nodes_ssl)';
    errors_ssl(iparam,itrial).conf=confusionmat(grn_target,est_target);

    [fuCMN_hard_vals, est_target]=max(fu_CMN,[],2);
    grn_target = f_target(target_unknown_nodes_ssl)';
    errors_ssl_cmn(iparam,itrial).conf = confusionmat(grn_target,est_target);
end

%% JGSA 
% Set algorithm parameters
options.k = 4;            % #subspace bases
options.ker = 'primal';     % kernel type, default='linear' options: linear, primal, gauss, poly

options.T = 30;             % #iterations, default=10

options.alpha = 0.55;           % the parameter for subspace divergence ||A-B||
options.mu = 48;             % the parameter for target variance
options.beta = 1;        % the parameter for P and Q (source discriminaiton)
options.kpar = 4;

est_target = JGSAClassify(  source_data(source_known_nodes,:), f_source(source_known_nodes)', ...
    target_data(target_known_nodes,:),  f_target(target_known_nodes)',...
    target_data(target_unknown_nodes,:),options);
grn_target = f_target(target_unknown_nodes)';
errors_jgsa(iparam,itrial).conf=confusionmat(grn_target,est_target);

%% SCA 
if ~isempty(target_known_nodes)
    beta = 1;
    delta = 0.1;
    d = 5;
    
    [ est_target ] = SCAClassify(  source_data(source_known_nodes,:), (f_source(source_known_nodes)'), ...
        target_data(target_known_nodes,:),  (f_target(target_known_nodes)'),...
        target_data(target_unknown_nodes,:),beta,delta,d)  ;
    grn_target = f_target(target_unknown_nodes)';
    errors_sca(iparam,itrial).conf=confusionmat(grn_target,est_target);
end
%% LDADA
[ est_target ] = LDADAClassify(  source_data(source_known_nodes,:), (f_source(source_known_nodes)'), ...
    target_data(target_known_nodes,:),  (f_target(target_known_nodes)'),...
    target_data(target_unknown_nodes,:),13);
grn_target = f_target(:,target_unknown_nodes)';
errors_ldada(iparam,itrial).conf=confusionmat(grn_target,est_target);

%% DASGA

if ~isempty(target_known_nodes)
        
    DASGA_params.mu = 0.15;
    DASGA_params.N_eigenvectors = 7;
    DASGA_params.N_iterations = 16;
    DASGA_params.mu_2 = 0.1; 

    DASGA_params.f_target = f_target'; %MODIFY ??
    DASGA_params.f_source = f_source';
    DASGA_params.labels = unique(DASGA_params.f_source(~isnan(DASGA_params.f_source)));
    DASGA_params.labeled_target_indices = target_known_nodes;
    DASGA_params.labeled_source_indices=source_known_nodes;
    DASGA_params.unlabeled_indices_target = target_unknown_nodes;
    DASGA_params.W = W1;
    DASGA_params.W_target = W2;

    pred_target=DASGA( DASGA_params, 1);
    assert(length(unique(pred_target)) <= 2)
    errors_dasga(iparam,itrial).conf=confusionmat(grn_target,pred_target);

end