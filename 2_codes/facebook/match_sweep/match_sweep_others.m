clear all
close all
clc
warning off

addpath('..')
addpath(genpath('../../functions'))

%% LOAD DATA
load ../../../1_data/facebook/source_data
load ../../../1_data/facebook/target_data

% Set weight matrices of the graphs
load ../../../1_data/facebook/W_source
load ../../../1_data/facebook/W_target

W1 = W_source;
W2 = W_target;

load ../../../1_data/facebook/gender_label_source
load ../../../1_data/facebook/gender_label_target

f_source = zeros(1,size(gender_label_source,1));
for pernode =1 : size(gender_label_source,1)
    label = find(gender_label_source(pernode,:) == 1);
    if(~isempty(label))
        f_source(pernode) = label;
    else
        f_source(pernode) = NaN;
    end
end
nolabelnodes_source = find(isnan(f_source));

f_target = zeros(1,size(gender_label_target,1));
for pernode =1 : size(gender_label_target,1)
    label = find(gender_label_target(pernode,:) == 1);
    if(~isempty(label))
        f_target(pernode) = label;
    else
        f_target(pernode) = NaN;
    end
end
nolabelnodes_target = find(isnan(f_target));



numofNodeSource = size(f_source,2);
numofNodeTarget = size(f_target,2);

%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/facebook/match/NodeStruct.mat
load ../../../3_outputs/facebook/Dict9.mat
W_source = Dict;
load ../../../3_outputs/facebook/Dict10.mat
W_target = Dict;
numberofWavelets = 5;

addpath("../../wda_functions")
paramlength = length(NodeStruct(1).targetknownnodes);


%% Hyperparameters
mu =(10^(0)); 
gama_t =(10^(-1));
gama_s = (10^(-1));


%% Experiments
ntrial = length(NodeStruct);
% errors_wda = struct(paramlength,ntrial)
for itrial = 1 : ntrial
    for iparam = 1 : paramlength
        display(["itrial" itrial "iparam" iparam])
        drawnow
        %% Get parameter setting
        target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
        source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
        match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
        match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
        
        target_known_nodes_ssl = target_known_nodes;
        target_unknown_nodes_ssl = setdiff(1:numofNodeTarget,target_known_nodes);
        assert(isempty(intersect(target_known_nodes_ssl,nolabelnodes_target)));

        
        % Delete nodes with no labels 
        target_known_nodes = setdiff(target_known_nodes,nolabelnodes_target);
        assert(isempty(intersect(target_known_nodes,nolabelnodes_target)));
        
        source_known_nodes = setdiff(source_known_nodes,nolabelnodes_source);
        assert(isempty(intersect(source_known_nodes,nolabelnodes_source)));
        
        % Check statistics
        display(["target_known_nodes" length(unique(target_known_nodes)) length((target_known_nodes))])
        assert(length(unique(target_known_nodes))==length((target_known_nodes)) )
        
        display(["source_known_nodes" length(unique(source_known_nodes)) length((source_known_nodes))])
        assert(length(unique(source_known_nodes))==length((source_known_nodes)) )

        display(["match_source_nodes" length(unique(match_source_nodes)) length((match_source_nodes))])        
        assert(length(unique(match_source_nodes))==length((match_source_nodes)) )
        
%         display(["match_target_nodes" length(unique(match_target_nodes)) length((match_target_nodes))])        
        assert(length(unique(match_target_nodes))==length((match_target_nodes)) )
        
        display(["labeled matched ratio" length(intersect(source_known_nodes,match_source_nodes))/length(match_source_nodes)])
        display(["labeled matched ratio" length(intersect(target_known_nodes,match_target_nodes))/length(match_target_nodes)])
        %% Reduce the dictionaries to matches
        dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
        dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
        

        target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
        target_unknown_nodes = setdiff(target_unknown_nodes,nolabelnodes_target);
        assert(isempty(intersect(target_unknown_nodes,nolabelnodes_target)));

        assert(~any(isnan(f_source(source_known_nodes))))
        assert(~any(isnan(f_target(target_known_nodes))))
        
        run("../runothermethods.m")
        
        
    end
end

save ../../../3_outputs/facebook/match/errors_svm_st errors_svm_st
save ../../../3_outputs/facebook/match/errors_nn_st errors_nn_st
save ../../../3_outputs/facebook/match/errors_sa errors_sa
save ../../../3_outputs/facebook/match/errors_ea errors_ea
save ../../../3_outputs/facebook/match/errors_gfk errors_gfk
% save ../../../3_outputs/facebook/match/errors_ssl_cmn errors_ssl_cmn
% save ../../../3_outputs/facebook/match/errors_ssl errors_ssl
save ../../../3_outputs/facebook/match/errors_jgsa errors_jgsa
save ../../../3_outputs/facebook/match/errors_ldada errors_ldada
% save ../../../3_outputs/facebook/match/errors_sca errors_sca