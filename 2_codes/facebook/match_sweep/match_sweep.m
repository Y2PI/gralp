clear all
close all
clc

addpath('..')
addpath(genpath('../../wda_functions'))

%% LOAD DATA

% Set weight matrices of the graphs
load ../../../1_data/facebook/W_source
load ../../../1_data/facebook/W_target

W1 = W_source;
W2 = W_target;

load ../../../1_data/facebook/gender_label_source
load ../../../1_data/facebook/gender_label_target

f_source = zeros(1,size(gender_label_source,1));
for pernode =1 : size(gender_label_source,1)
    label = find(gender_label_source(pernode,:) == 1);
    if(~isempty(label))
        f_source(pernode) = label;
    else
        f_source(pernode) = NaN;
    end
end
nolabelnodes_source = find(isnan(f_source));

f_target = zeros(1,size(gender_label_target,1));
for pernode =1 : size(gender_label_target,1)
    label = find(gender_label_target(pernode,:) == 1);
    if(~isempty(label))
        f_target(pernode) = label;
    else
        f_target(pernode) = NaN;
    end
end
nolabelnodes_target = find(isnan(f_target));

numofNodeSource = size(f_source,2);
numofNodeTarget = size(f_target,2);

%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/facebook/match/NodeStruct.mat
load ../../../3_outputs/facebook/Dict9.mat
W_source = Dict;
load ../../../3_outputs/facebook/Dict10.mat
W_target = Dict;
numberofWavelets = 5;

addpath("../../wda_functions")
paramlength = length(NodeStruct(1).targetknownnodes);


%% Hyperparameters
mu =(10^(0)); 
gama_t =(10^(-1));
gama_s = (10^(-1));

%% Experiments
ntrial = length(NodeStruct);
% errors_wda = struct(paramlength,ntrial)
for itrial = 1 : ntrial
    for iparam = 1 : paramlength
        display(["itrial" itrial "iparam" iparam])
        drawnow
        %% Get parameter setting
        target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
        source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
        match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
        match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
        
        % Check statistics
        display(["target_known_nodes" length(unique(target_known_nodes)) length((target_known_nodes))])
        assert(length(unique(target_known_nodes))==length((target_known_nodes)) )
        
        display(["source_known_nodes" length(unique(source_known_nodes)) length((source_known_nodes))])
        assert(length(unique(source_known_nodes))==length((source_known_nodes)) )

        display(["match_source_nodes" length(unique(match_source_nodes)) length((match_source_nodes))])        
        assert(length(unique(match_source_nodes))==length((match_source_nodes)) )
        
%         display(["match_target_nodes" length(unique(match_target_nodes)) length((match_target_nodes))])        
        assert(length(unique(match_target_nodes))==length((match_target_nodes)) )
        
        display(["labeled matched ratio" length(intersect(source_known_nodes,match_source_nodes))/length(match_source_nodes)])
        display(["labeled matched ratio" length(intersect(target_known_nodes,match_target_nodes))/length(match_target_nodes)])
        %% Reduce the dictionaries to matches
        dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
        dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
        
        y_s = f_source(source_known_nodes)';
        y_t = f_target(target_known_nodes)';    
        
        knownclasses = (unique([y_s; y_t]));
        knownclasses = knownclasses(~isnan(knownclasses));
        
        RTarget = zeros(numofNodeTarget,length(knownclasses));
        % ONE VS ALL Trainning
        for perclass = 1 : length(knownclasses) 
             l = knownclasses(perclass); 
             % ONE-HOT ENCODING
             KnownLabel_source = (y_s == l); 
             KnownLabel_target = (y_t == l);
             
             RTarget(:,perclass) = Gralp_facebook( numofNodeSource,numofNodeTarget, ... % num of nodes
                             mu,gama_s,gama_t,... %hyperparameters
                             dict,W1,W2,... % dictionaries and graphs
                             KnownLabel_source,KnownLabel_target,... % known labels
                             source_known_nodes,target_known_nodes ); % indexes
        end
        %% SOFT TO HARD LABEL ASSIGNMENT
        predicted_labels = zeros(1,numofNodeTarget);
        for pernode = 1 : numofNodeTarget
            [~,lblidx] = max(RTarget(pernode,:));
            predicted_labels(pernode) =  knownclasses(lblidx);
        end
        
        target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
        
        errors_wda(iparam,itrial).conf = confusionmat(f_target(target_unknown_nodes),predicted_labels(target_unknown_nodes));
        display([sum(sum(errors_wda(iparam,itrial).conf)),length(target_unknown_nodes),sum(isnan(f_target(target_unknown_nodes)))])
    end
end

save ../../../3_outputs/facebook/match/errors_wda errors_wda
