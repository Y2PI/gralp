clear all
close all
clc
%% PARAM SEARCH 
% A heuristic hyper-param search for algorithms 
% Source labels are mostly available, in target, small number of labels are
% given 


%% LOAD DATA
DataLoc = '../../../1_data/Multilingual';
load '../../../1_data/Multilingual/ENENWeight.mat'
load '../../../1_data/Multilingual/ENFRWeight.mat'

source_data = importdata('../../../1_data/Multilingual/EN2EN_norm_Reduced.mat')';
target_data = importdata('../../../1_data/Multilingual/EN2FR_norm_Reduced.mat')';

numofNodeSource = size(source_data,2);
numofNodeTarget = size(target_data,2);

% Set weight matrices of the graphs
W1 = ENENWeight;
W2 = ENFRWeight;

% Generate the labels
f_source = importdata('../../../1_data/Multilingual/EN2EN_Reduced_label.mat')+1;
f_target = importdata('../../../1_data/Multilingual/EN2FR_Reduced_label.mat')+1;



%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/Multilingual/target/NodeStruct.mat

% load ../../../3_outputs/Multilingual/Dict_Source.mat
% W_source = Dict;
% load ../../../3_outputs/Multilingual/Dict_Target.mat
% W_target = Dict;

load ./SourceDict_norm.mat
load ./TargetDict_norm.mat

addpath("../../wda_functions")
addpath(genpath("../../functions"))
addpath(genpath("../../sgwt_toolbox-1.02"))

numberofWavelets = 5;
% Dict = CreateWaveletDictionary(W1,numberofWavelets);
% W_source = Dict;
% 
% Dict = CreateWaveletDictionary(W2,numberofWavelets);
% W_target = Dict;

param = [1 10 15];
trial = [1 5 10];

% %% JGSA 
% 
% error_JGSA = 0;
% min_error_JGSA = 100*length(param)*length(trial)
% hparam1_mu = 20;
% hparam1_std = 5;
% 
% hparam2_mu = 20;
% hparam2_std = 10;
% 
% hparam3_mu = -2;
% hparam3_std = 2;
% 
% hparam4_mu = -2;
% hparam4_std = 2;
% 
% hparam5_mu = 3;
% hparam5_std = 3;
% 
% hparam6_mu = -3;
% hparam6_std = 3;
% 
% maxranditer = 10;
% 
% 
% hparam1 = round(randn()*hparam1_std + hparam1_mu)
% hparam2 = round(randn()*hparam2_std + hparam2_mu)
% hparam3 = randn()*hparam3_std + hparam3_mu
% hparam4 = randn()*hparam4_std + hparam4_mu
% hparam5 = randn()*hparam5_std + hparam5_mu
% hparam6 = randn()*hparam6_std + hparam6_mu
% 
% 
% for riter = 1 : maxranditer
%     error_JGSA = 0;
%     
%     for pertrial = 1 : 3
%     for perparam = 1 : 3
%         iparam = param(perparam);
%         itrial = trial(pertrial);
%         source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
%         target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
%         match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
%         match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
% 
%         y_s = f_source(source_known_nodes)';
%         y_t = f_target(target_known_nodes)';    
% 
%         target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
% 
%         options.k = hparam1;            % #subspace bases
%         options.ker = 'linear';     % kernel type, default='linear' options: linear, primal, gauss, poly
% 
%         options.T = hparam2;             % #iterations, default=10
% 
%         options.alpha= 10^hparam3;           % the parameter for subspace divergence ||A-B||
%         options.mu = 10^hparam4;             % the parameter for target variance
%         options.beta = 10^hparam5;        % the parameter for P and Q (source discriminaiton)
%         options.kpar = 10^hparam6;
% 
%         pred_target = JGSAClassify(  source_data(:,source_known_nodes)',...
%             y_s, target_data(:,target_known_nodes)', y_t,...
%             target_data(:,target_unknown_nodes)',options);
% 
%         acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
%         error_JGSA = error_JGSA + 100 - 100*acc;
% 
% 
%     end
%     end
%     if(min_error_JGSA > error_JGSA)
%         min_error_hparam_JGSA1 = hparam1
%         min_error_hparam_JGSA2 = hparam2
%         min_error_hparam_JGSA3 = hparam3
%         min_error_hparam_JGSA4 = 10^hparam4
%         min_error_hparam_JGSA5 = 10^hparam5
%         min_error_hparam_JGSA6 = 10^hparam6
% 
%         min_error_JGSA = error_JGSA
%     end
%     
%     hparam1 = round(randn()*hparam1_std/riter + min_error_hparam_JGSA1);
%     hparam2 = round(randn()*hparam2_std/riter + min_error_hparam_JGSA2);
%     hparam3 = randn()*hparam3_std + min_error_hparam_JGSA3;
%     hparam4 = randn()*hparam4_std + min_error_hparam_JGSA4;
%     hparam5 = randn()*hparam5_std + min_error_hparam_JGSA5;
%     hparam6 = randn()*hparam6_std + min_error_hparam_JGSA6;
% 
% end


%% DASGA 

error_DASGA = 0 
min_error_DASGA = 100*length(param)*length(trial)
hparam1_mu = -2;
hparam1_std = 2;

hparam2_mu = 10;
hparam2_std = 3;

hparam3_mu = 10;
hparam3_std = 3;

hparam4_mu = -2;
hparam4_std = 2;


hparam1 = (randn()*hparam1_std + hparam1_mu)
hparam2 = round(randn()*hparam2_std + hparam2_mu)
hparam3 = round(randn()*hparam3_std + hparam3_mu)
hparam4 = randn()*hparam4_std + hparam4_mu

maxranditer = 10;


for riter = 1 : maxranditer
        error_DASGA = 0;
        for pertrial = 1 : 3

            for perparam = 1 : 3
                iparam = param(perparam);
                itrial = trial(pertrial);
                source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
                target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
                match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
                match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};

                y_s = f_source(source_known_nodes)';
                y_t = f_target(target_known_nodes)';    

                target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);


                DASGA_params.f_target = f_target'; %MODIFY ??
                DASGA_params.f_source = f_source';
                DASGA_params.mu = 10^hparam1;
                DASGA_params.N_eigenvectors = hparam2;
                DASGA_params.N_iterations = hparam3;
                DASGA_params.mu_2 = 10^hparam4; 

                DASGA_params.labels = unique(DASGA_params.f_source);
                DASGA_params.labeled_target_indices = target_known_nodes;
                DASGA_params.labeled_source_indices=source_known_nodes;
                DASGA_params.W = W1;
                DASGA_params.W_target = W2;

                pred_target=DASGA( DASGA_params, 1);       

                acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
                error_DASGA = error_DASGA + 100 - 100*acc;
            end
        end
        if(min_error_DASGA > error_DASGA)
            min_error_hparam_DASGA1 = 10^hparam1
            min_error_hparam_DASGA2 = hparam2
            min_error_hparam_DASGA3 = hparam3
            min_error_hparam_DASGA4 = 10^hparam4

            min_error_DASGA = error_DASGA
        end
        hparam1 = (randn()*hparam1_std + min_error_hparam_DASGA1);
        hparam2 = round(randn()*hparam2_std + min_error_hparam_DASGA2);
        hparam3 = round(randn()*hparam3_std + min_error_hparam_DASGA3);
        hparam4 = randn()*hparam4_std + min_error_hparam_DASGA4;

end
