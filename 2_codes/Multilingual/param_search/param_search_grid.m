clear all
close all
clc
%% PARAM SEARCH 
% A heuristic hyper-param search for algorithms 
% Source labels are mostly available, in target, small number of labels are
% given 


%% LOAD DATA
DataLoc = '../../../1_data/Multilingual';
load '../../../1_data/Multilingual/ENENWeight.mat'
load '../../../1_data/Multilingual/ENFRWeight.mat'

source_data = importdata('../../../1_data/Multilingual/EN2EN_norm_Reduced.mat')';
target_data = importdata('../../../1_data/Multilingual/EN2FR_norm_Reduced.mat')';

numofNodeSource = size(source_data,2);
numofNodeTarget = size(target_data,2);

% Set weight matrices of the graphs
W1 = ENENWeight;
W2 = ENFRWeight;

% Generate the labels
f_source = importdata('../../../1_data/Multilingual/EN2EN_Reduced_label.mat')+1;
f_target = importdata('../../../1_data/Multilingual/EN2FR_Reduced_label.mat')+1;



%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/Multilingual/target/NodeStruct.mat

% load ../../../3_outputs/Multilingual/Dict_Source.mat
% W_source = Dict;
% load ../../../3_outputs/Multilingual/Dict_Target.mat
% W_target = Dict;

load ./SourceDict_norm.mat
load ./TargetDict_norm.mat

addpath("../../wda_functions")
addpath(genpath("../../functions"))
addpath(genpath("../../sgwt_toolbox-1.02"))

numberofWavelets = 5;
% Dict = CreateWaveletDictionary(W1,numberofWavelets);
% W_source = Dict;
% 
% Dict = CreateWaveletDictionary(W2,numberofWavelets);
% W_target = Dict;

trial = [4 7 10]; 
param = [1 5 7];

error_SA = 0;
min_error_SA = 100*length(param)*length(trial);
%% SA 
for hparam = 1:2:40
        error_SA = 0;

        for pertrial = 1 : 3
            for perparam = 1 : 3
                iparam = param(perparam);
                itrial = trial(pertrial);
                source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
                target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
                match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
                match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};

                y_s = f_source(source_known_nodes)';
                y_t = f_target(target_known_nodes)';    

                target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);

                source_unknown_nodes = setdiff(1:size(source_data,2),source_known_nodes);
                pred_target = SAClassify(  source_data(:,source_known_nodes)',...
                             y_s, source_data(:,source_unknown_nodes)',...
                              target_data(:,target_known_nodes)', y_t, ...
                              target_data(:,target_unknown_nodes)', hparam, 'nn');

                acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
                error_SA = error_SA + 100 - 100*acc;

            end
        
        end
        if(min_error_SA > error_SA)
            min_error_hparam_SA = hparam
            min_error_SA = error_SA
        end
end

%% SCA 
error_SCA = 0 ;
min_error_SCA = 100*length(param)*length(trial);
for hparam1 = -4:1:0
    for hparam2 = 1 : 6
        for hparam3 = 1 :2: 21
            error_SCA = 0;
            for pertrial = 1 : 3
                for perparam = 1 : 3
                    iparam = param(perparam);
                    itrial = trial(pertrial);
                    source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
                    target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
                    match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
                    match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};

                    y_s = f_source(source_known_nodes)';
                    y_t = f_target(target_known_nodes)';    

                    target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);




                    pred_target = SCAClassify( source_data(:,source_known_nodes)', y_s, ...
                    target_data(:,target_known_nodes)', y_t,...
                    target_data(:,target_unknown_nodes)',10^hparam1, 10^hparam2, hparam3);

                    acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
                    error_SCA = error_SCA + 100 - 100*acc;
                end
            end
            if(min_error_SCA > error_SCA)
                min_error_hparam_SCA1 = 10^hparam1
                min_error_hparam_SCA2 = 10^hparam2
                min_error_hparam_SCA3 = hparam3
                min_error_SCA = error_SCA
            end

        end
    end
end

%% JGSA
% 
% error_JGSA = 0;
% min_error_JGSA = 100*length(param)*length(trial)
% for hparam1 = 20:40:180
%     for hparam2 = 10:10:50
%         for hparam3 = -2:1: 0
%             for hparam4 = -3:1:0
%                 for hparam5 = 1 : 6
%                     for hparam6 = -5 :1: 0
%                         error_JGSA = 0;
%                         for pertrial = 1 : 3
%                         for perparam = 1 : 3
%                             iparam = param(perparam);
%                             itrial = trial(pertrial);
%                             source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
%                             target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
%                             match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
%                             match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
% 
%                             y_s = f_source(source_known_nodes)';
%                             y_t = f_target(target_known_nodes)';    
% 
%                             target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
% 
%                             options.k = hparam1;            % #subspace bases
%                             options.ker = 'linear';     % kernel type, default='linear' options: linear, primal, gauss, poly
% 
%                             options.T = hparam2;             % #iterations, default=10
% 
%                             options.alpha= 10^hparam3;           % the parameter for subspace divergence ||A-B||
%                             options.mu = 10^hparam4;             % the parameter for target variance
%                             options.beta = 10^hparam5;        % the parameter for P and Q (source discriminaiton)
%                             options.kpar = 10^hparam6;
% 
%                             pred_target = JGSAClassify(  source_data(:,source_known_nodes)',...
%                                 y_s, target_data(:,target_known_nodes)', y_t,...
%                                 target_data(:,target_unknown_nodes)',options);
% 
%                             acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
%                             error_JGSA = error_JGSA + 100 - 100*acc;
% 
%                             
%                         end
%                         end
%                         if(min_error_JGSA > error_JGSA)
%                             min_error_hparam_JGSA1 = hparam1
%                             min_error_hparam_JGSA2 = hparam2
%                             min_error_hparam_JGSA3 = hparam3
%                             min_error_hparam_JGSA4 = 10^hparam4
%                             min_error_hparam_JGSA5 = 10^hparam5
%                             min_error_hparam_JGSA6 = 10^hparam6
% 
%                             min_error_JGSA = error_JGSA
%                         end
%                     end
%                 end
%             end
%         end
%     end
% end
%% LDADA 

error_LDADA = 0
min_error_LDADA = 100*length(param)*length(trial)

for hparam = 1:1:40
        error_LDADA = 0;

        for pertrial = 1 : 3
            for perparam = 1 : 3
                iparam = param(perparam);
                itrial = trial(pertrial);
                source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
                target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
                match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
                match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};

                y_s = f_source(source_known_nodes)';
                y_t = f_target(target_known_nodes)';    

                target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);

                source_unknown_nodes = setdiff(1:size(source_data,2),source_known_nodes);
                pred_target = LDADAClassify( source_data(:,source_known_nodes)', y_s, ...
                    target_data(:,target_known_nodes)', y_t,...
                    target_data(:,target_unknown_nodes)',hparam);
                acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
                error_LDADA = error_LDADA + 100 - 100*acc;

            end
        
        end
        error_LDADA
        if(min_error_LDADA > error_LDADA)
            min_error_hparam_LDADA = hparam
            min_error_LDADA = error_LDADA
        end
end

%% DASGA 
% 
% error_DASGA = 0 
% min_error_DASGA = 100*length(param)*length(trial)
% for hparam1 = -4 :1: 0
%     for hparam2 = 2:2:20
%         for hparam3 = 2:2:20
%             for hparam4 = -4:1:0
%                 error_DASGA = 0;
%                 for pertrial = 1 : 3
% 
%                     for perparam = 1 : 3
%                         iparam = param(perparam);
%                         itrial = trial(pertrial);
%                         source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
%                         target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
%                         match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
%                         match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
% 
%                         y_s = f_source(source_known_nodes)';
%                         y_t = f_target(target_known_nodes)';    
% 
%                         target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
% 
% 
%                         DASGA_params.f_target = f_target'; %MODIFY ??
%                         DASGA_params.f_source = f_source';
%                         DASGA_params.mu = 10^hparam1;
%                         DASGA_params.N_eigenvectors = hparam2;
%                         DASGA_params.N_iterations = hparam3;
%                         DASGA_params.mu_2 = 10^hparam4; 
% 
%                         DASGA_params.labels = unique(DASGA_params.f_source);
%                         DASGA_params.labeled_target_indices = target_known_nodes;
%                         DASGA_params.labeled_source_indices=source_known_nodes;
%                         DASGA_params.W = W1;
%                         DASGA_params.W_target = W2;
% 
%                         pred_target=DASGA( DASGA_params, 1);       
% 
%                         acc= sum(pred_target' == f_target(target_unknown_nodes))/length(target_unknown_nodes);
%                         error_DASGA = error_DASGA + 100 - 100*acc;
%                     end
%                 end
%                 if(min_error_DASGA > error_DASGA)
%                     min_error_hparam_DASGA1 = 10^hparam1
%                     min_error_hparam_DASGA2 = hparam2
%                     min_error_hparam_DASGA3 = hparam3
%                     min_error_hparam_DASGA4 = 10^hparam4
% 
%                     min_error_DASGA = error_DASGA
%                 end
%             end
%         end
%     end
% end
% 
% %% Gralp 
% 
% 
% error_WDA = 0 
% min_error_WDA = 100*length(param)*length(trial)
% for hparam1 = -3 :1: 0
%     for hparam2 =-3 :1: 0
%         for hparam3 = -3 :1: 0
%             
%                 error_WDA = 0;
%                 for pertrial = 1 : 3
% 
%                     for perparam = 1 : 3
%                         iparam = param(perparam);
%                         itrial = trial(pertrial);
%                         source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
%                         target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
%                         match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
%                         match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
% 
%                         y_s = f_source(source_known_nodes)';
%                         y_t = f_target(target_known_nodes)';    
% 
%                         target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
% 
% 
%                 %% Reduce the dictionaries to matches
%                         dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
%                         dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
% 
%                         y_s = f_source(source_known_nodes)';
%                         y_t = f_target(target_known_nodes)';    
% 
%                         knownclasses = (unique([y_s; y_t]));
% 
%                         RTarget = zeros(numofNodeTarget,length(knownclasses));
%                         % ONE VS ALL Trainning
%                         for perclass = 1 : length(knownclasses) 
%                              l = knownclasses(perclass); 
%                              % ONE-HOT ENCODING
%                              KnownLabel_source = (y_s == l); 
%                              KnownLabel_target = (y_t == l);
% 
%                              RTarget(:,perclass) = Gralp( numofNodeSource,numofNodeTarget, ... % num of nodes
%                                              10^hparam1,10^hparam2,10^hparam3,... %hyperparameters
%                                              dict,W1,W2,... % dictionaries and graphs
%                                              KnownLabel_source,KnownLabel_target,... % known labels
%                                              source_known_nodes,target_known_nodes ); % indexes
%                         end
%                         %% SOFT TO HARD LABEL ASSIGNMENT
%                         predicted_labels = zeros(1,numofNodeTarget);
%                         for pernode = 1 : numofNodeTarget
%                             [~,lblidx] = max(RTarget(pernode,:));
%                             predicted_labels(pernode) =  knownclasses(lblidx);
%                         end
% 
%                         target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
%                         pred_target = predicted_labels(target_unknown_nodes);
%                         
%                         acc= sum(pred_target == f_target(target_unknown_nodes))/length(target_unknown_nodes);
%                         error_WDA = error_WDA + 100 - 100*acc ;
%                     end
%                 end
%                 if(min_error_WDA > error_WDA)
%                     min_error_hparam_WDA1 = 10^hparam1
%                     min_error_hparam_WDA2 = 10^hparam2
%                     min_error_hparam_WDA3 = 10^hparam3
% 
%                     min_error_WDA = error_WDA
%                 end
%         end
%     end
% end