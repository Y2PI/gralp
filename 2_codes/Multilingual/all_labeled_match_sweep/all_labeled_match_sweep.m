clear all
close all
clc

%% LOAD DATA
DataLoc = '../../../1_data/Multilingual';
load '../../../1_data/Multilingual/ENENWeight.mat'
load '../../../1_data/Multilingual/ENFRWeight.mat'

source_data = importdata('../../../1_data/Multilingual/EN2EN_Reduced.mat');
target_data = importdata('../../../1_data/Multilingual/EN2FR_Reduced.mat');

% Preprocess on data
source_data= source_data'./vecnorm(source_data');
target_data= target_data'./vecnorm(target_data');

numofNodeSource = size(source_data,2);
numofNodeTarget = size(target_data,2);

% Set weight matrices of the graphs
W1 = ENENWeight;
W2 = ENFRWeight;

% Generate the labels
f_source = zeros(1,1500);
for i = 1 : 6
    f_source(((i-1)*250+1):(i*250))= i ;
end

f_target = zeros(1,1500);
for i = 1 : 6
    f_target(((i-1)*250+1):(i*250))= i ;
end



%% LOAD DICTIONARIES AND NODESTRUCT
load ../../../3_outputs/Multilingual/partially_match/NodeStruct.mat
load ../../../3_outputs/Multilingual/Dict_Source.mat
W_source = Dict;
load ../../../3_outputs/Multilingual/Dict_Target.mat
W_target = Dict;
numberofWavelets = 5;

addpath("../../../wda_functions")
paramlength = length(NodeStruct(1).targetknownnodes);


%% Hyperparameters
mu =(10^(0)); 
gama_t =(10^(-1));
gama_s = (10^(-1));

%% Experiments
ntrial = length(NodeStruct);
errors_wda = zeros(paramlength,ntrial)
for itrial = 1 : ntrial
    for iparam = 1 : paramlength
        display(["itrial" itrial "iparam" iparam])
        drawnow
        %% Get parameter setting
        target_known_nodes = NodeStruct(itrial).targetknownnodes{iparam};
        source_known_nodes = NodeStruct(itrial).sourceknownnodes{iparam};
        match_source_nodes = NodeStruct(itrial).matchsourcenodes{iparam};
        match_target_nodes = NodeStruct(itrial).matchtargetnodes{iparam};
        
        % Check statistics
        display(["target_known_nodes" length(unique(target_known_nodes)) length((target_known_nodes))])
        assert(length(unique(target_known_nodes))==length((target_known_nodes)) )
        
        display(["source_known_nodes" length(unique(source_known_nodes)) length((source_known_nodes))])
        assert(length(unique(source_known_nodes))==length((source_known_nodes)) )

        display(["match_source_nodes" length(unique(match_source_nodes)) length((match_source_nodes))])        
        assert(length(unique(match_source_nodes))==length((match_source_nodes)) )
        
        display(["match_target_nodes" length(unique(match_target_nodes)) length((match_target_nodes))])        
        assert(length(unique(match_target_nodes))==length((match_target_nodes)) )
        
        display(["labeled matched ratio" length(intersect(source_known_nodes,match_source_nodes))/length(match_source_nodes)])
        display(["labeled matched ratio" length(intersect(target_known_nodes,match_target_nodes))/length(match_target_nodes)])
        %% Reduce the dictionaries to matches
        dict{1} = ReduceDictionary(numberofWavelets,W_source,match_source_nodes);
        dict{2} = ReduceDictionary(numberofWavelets,W_target,match_target_nodes);
        
        y_s = f_source(source_known_nodes)';
        y_t = f_target(target_known_nodes)';    

               knownclasses = (unique([y_s; y_t]));
        
        RTarget = zeros(numofNodeTarget,length(knownclasses));
        % ONE VS ALL Trainning
        for perclass = 1 : length(knownclasses) 
             l = knownclasses(perclass); 
             % ONE-HOT ENCODING
             KnownLabel_source = (y_s == l); 
             KnownLabel_target = (y_t == l);
             
             RTarget(:,perclass) = Gralp( numofNodeSource,numofNodeTarget, ... % num of nodes
                             mu,gama_s,gama_t,... %hyperparameters
                             dict,W1,W2,... % dictionaries and graphs
                             KnownLabel_source,KnownLabel_target,... % known labels
                             source_known_nodes,target_known_nodes ); % indexes
        end
        %% SOFT TO HARD LABEL ASSIGNMENT
        predicted_labels = zeros(1,numofNodeTarget);
        for pernode = 1 : numofNodeTarget
            [~,lblidx] = max(RTarget(pernode,:));
            predicted_labels(pernode) =  knownclasses(lblidx);
        end
        
        target_unknown_nodes = setdiff(1 : numofNodeTarget, target_known_nodes);
        acc_count = sum(predicted_labels(target_unknown_nodes) == f_target(target_unknown_nodes));
        errors_wda(iparam,itrial) = 100*(1-acc_count/ length(target_unknown_nodes));    
        
    end
end

save ../../../3_outputs/Multilingual/partially_match/errors_wda errors_wda
