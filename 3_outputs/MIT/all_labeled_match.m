clear all
% close all
axesFontSize = 18;
legendFontSize = 14;
legendNumColumns = 1;
LineWidth = 1.5;
MarkerSize= 6;

load all_labeled_match/NodeStruct.mat

load all_labeled_match/errors_wda.mat
% load match_part_labeled/errors_reg.mat

load all_labeled_match/errors_svm_st.mat
load all_labeled_match/errors_nn_st.mat
load all_labeled_match/errors_sa.mat
% load all_labeled_match/errors_ea.mat
load all_labeled_match/errors_gfk.mat
load all_labeled_match/errors_ssl.mat
load all_labeled_match/errors_ssl_CMN.mat
load all_labeled_match/errors_jgsa.mat
load all_labeled_match/errors_ldada.mat
load all_labeled_match/errors_sca.mat
load all_labeled_match/errors_dasga.mat


for(j=1:size(NodeStruct(1).sourceknownnodes,2))
    %number of nodes in source and target domain  = 360
    param_vect(j) = 100*length(NodeStruct(1).matchsourcenodes{j})/360;
end


h = figure;
box on
colors= [0,0,1;... WDA
    0 1 0;...SVM S + T
    0.7 0.1 0.7;... NN S +T
    %     0.4 0 0.8;0 0.2 0.5;0 1 0;... SVM ST SVM T SVM S
    %     0.6 0 0.2;1 0.5 0;0.7 0.1 0.7;... NN ST NN T NN S
    0 0 0;... SA
%     1 0 0.5;... EA ++
    0 0.6  1;...GFK
    0.4 0.4 0.6;...SSL
    1 0 0 ;...JGSA
    0 0.5 0;...LDADA
%     0 0.5 0.5];...SCA
    1 0.5 0; ... SCA
    1 0 0.5]; ... DASGA
    set(gca, 'ColorOrder', colors, 'NextPlot', 'replacechildren');


plot(param_vect, mean(errors_wda,2),'*-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
hold on
plot(param_vect, mean(errors_svm_st,2), 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_nn_st,2), 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_sa,2), 'x-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
% plot(param_vect, mean(errors_ea,2), 'v-',...
%     'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_gfk,2), '<-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_ssl_CMN,2), 'd-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,mean(errors_jgsa,2),'>-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,mean(errors_ldada,2),'x--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,mean(errors_sca,2),'s--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,mean(errors_dasga,2),'o--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);

ylim([0,100])
xlim([0,20])
xlabel('Ratio of matched nodes (%)');
ylabel('Misclassification rate (%)');

L = legend('WDA', 'SVM:S+T','NN:S+T','SA',...'EA++',...
    'GFK','SSL','JGSA','LDADA','SCA','DASGA','FontSize',legendFontSize,'NumColumns',legendNumColumns);
L.Position = [0.6585    0.3182    0.2354    0.5917];
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
set(gca,'fontsize',axesFontSize)

print(gcf,'MIT_all_labeled_corrected.pdf','-dpdf','-r0')
saveas(gcf,'MIT_all_labeled_corrected.fig')