clear all
% close all
axesFontSize = 18;
legendFontSize = 14;
legendNumColumns = 2;
LineWidth = 1.5;
MarkerSize= 6;
load match/NodeStruct.mat

load match/errors_wda.mat
load match/errors_svm_st.mat
load match/errors_nn_st.mat
load match/errors_sa.mat
load match/errors_ea.mat
load match/errors_gfk.mat
load match/errors_jgsa.mat
load match/errors_ldada.mat

 for(j=1:size(NodeStruct(1).sourceknownnodes,2))
    %number of nodes in source and target domain  = 360
    param_vect(j) = 100*length(NodeStruct(1).matchsourcenodes{j})/360; 
 end
% Number of known target labels

h = figure;
box on
colors= [0,0,1;...WDA
    0 1 0;...SVM S
    0.7 0.1 0.7;... NN S
    0 0 0;...SA
    1 0.3 0.6;...EA++
    0 0.6  1;...GFK
    1 0 0  ;...JGSA
    0 0.5 0;]; ... LDADA
set(gca, 'ColorOrder', colors, 'NextPlot', 'replacechildren');

plot(param_vect, mean(errors_wda,2),'*-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
hold on
plot(param_vect, mean(errors_svm_st,2), 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_nn_st,2), 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_sa,2), 'x-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_ea,2), 'v-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, mean(errors_gfk,2), '<-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,mean(errors_jgsa,2),'>-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,mean(errors_ldada,2),'x--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	
	
	
ylim([0,100])
xlabel('Ratio of matched nodes (%)');
ylabel('Misclassification rate (%)');
L = legend('WDA', 'SVM:S+T', 'NN:S+T','SA','EA++','GFK','JGSA','LDADA','FontSize',legendFontSize,'NumColumns',legendNumColumns);
L.Position = [0.4574    0.6719    0.4379    0.2417];

set(gcf,'Units','Inches');
 pos = get(gcf,'Position');
  set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
set(gca,'fontsize',axesFontSize)

print(gcf,'MIT_match_corrected.pdf','-dpdf','-r0')
saveas(gcf,'MIT_match_corrected.fig')