function [ber] = conf2ber(conf)
% Takes 2x2 confusion matrix and calculate the balanced error rate
ber = 0.5*(conf(1,1)/ (conf(1,1) + conf(1,2))) + 0.5*(conf(2,2)/ (conf(2,1) + conf(2,2)));
end

