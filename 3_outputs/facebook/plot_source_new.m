clear all
% close all
clc
load source/NodeStruct.mat

load source/errors_wda.mat
load source/errors_svm_st.mat
load source/errors_nn_st.mat
load source/errors_sa.mat
load source/errors_ea.mat
load source/errors_gfk.mat
load source/errors_jgsa.mat
load source/errors_ldada.mat

 for(j=1:size(NodeStruct(1).sourceknownnodes,2))
    param_vect(j) = 100*length(NodeStruct(1).sourceknownnodes{j})/168;
end
% Number of known target labels
LineWidth = 1.5;
MarkerSize= 6;
h = figure;
box on
colors= [0,0,1;...WDA
    0 1 0;...SVM S
    0.7 0.1 0.7;... NN S
    0 0 0;...SA
    1 0 0.5;... EA ++
    0 0.6  1;...GFK
    1 0 0  ;...JGSA
    0 0.5 0;]; ... LDADA
    
set(gca, 'ColorOrder', colors, 'NextPlot', 'replacechildren');

 
ber_wda = zeros(size(errors_wda,1),1); 
ber_svm_st = zeros(size(errors_svm_st,1),1); 
ber_nn_st = zeros(size(errors_nn_st,1),1); 
ber_ea = zeros(size(errors_ea,1),1); 
ber_sa = zeros(size(errors_sa,1),1); 
ber_ldada = zeros(size(errors_ldada,1),1); 
ber_jgsa = zeros(size(errors_jgsa,1),1); 
ber_gfk = zeros(size(errors_gfk,1),1); 


for i = 1 : size(errors_svm_st,1)
    for j = 1 : size(errors_svm_st,2)
        ber_wda(i) = ber_wda(i)+ conf2ber(errors_wda(i,j).conf);
        ber_svm_st(i) = ber_svm_st(i)+ conf2ber(errors_svm_st(i,j).conf);
        ber_nn_st(i) = ber_nn_st(i)+ conf2ber(errors_nn_st(i,j).conf);
        ber_ea(i) = ber_ea(i)+ conf2ber(errors_ea(i,j).conf);
        ber_sa(i) = ber_sa(i)+ conf2ber(errors_sa(i,j).conf);
        ber_ldada(i) = ber_ldada(i)+ conf2ber(errors_ldada(i,j).conf);
        ber_jgsa(i) = ber_jgsa(i)+ conf2ber(errors_jgsa(i,j).conf);
        ber_gfk(i) = ber_gfk(i)+ conf2ber(errors_gfk(i,j).conf);
    end
end
ber_wda = ber_wda ./ size(errors_wda,2);
ber_svm_st = ber_svm_st ./ size(errors_svm_st,2);
ber_nn_st = ber_nn_st ./ size(errors_nn_st,2);
ber_ea = ber_ea ./ size(errors_ea,2);
ber_sa = ber_sa ./ size(errors_sa,2);
ber_ldada = ber_ldada ./ size(errors_ldada,2);
ber_jgsa = ber_jgsa ./ size(errors_jgsa,2);
ber_gfk = ber_gfk ./ size(errors_gfk,2);


plot(param_vect, 100- 100*ber_wda,'*-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
hold on

plot(param_vect, 100- 100* ber_svm_st, 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);

plot(param_vect,100- 100*ber_nn_st, '^--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*ber_sa, 'x-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*ber_ea, 'v-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_gfk, '<-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_jgsa,'>-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_ldada,'x--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	
ylim([30,65])

xlabel('Ratio of known source labels (%)','FontSize',16);
ylabel('Misclassification rate (%)','FontSize',16);
L = legend('GrALP', 'SVM', 'NN','SA','EA++','GFK','JGSA','LDADA','FontSize',14,'NumColumns',1);

set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
set(gca,'fontsize',18)

print(gcf,'Facebook_source_c.pdf','-dpdf','-r0')
saveas(gcf,'Facebook_source_c.fig')