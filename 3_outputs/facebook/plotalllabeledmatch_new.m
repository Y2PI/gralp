clear all

load all_labeled_match/NodeStruct.mat

load all_labeled_match/errors_wda.mat
load all_labeled_match/errors_svm_st.mat
load all_labeled_match/errors_nn_st.mat
load all_labeled_match/errors_sa.mat
load all_labeled_match/errors_ea.mat
load all_labeled_match/errors_gfk.mat
load all_labeled_match/errors_ssl.mat
load all_labeled_match/errors_ssl_cmn.mat
load all_labeled_match/errors_jgsa.mat
load all_labeled_match/errors_ldada.mat
load all_labeled_match/errors_sca.mat
load all_labeled_match/errors_dasga.mat

 for(j=1:size(NodeStruct(1).sourceknownnodes,2))
    param_vect(j) = 100*length(NodeStruct(1).matchsourcenodes{j})/61;
end

% Number of known target labels
LineWidth = 1.5;
MarkerSize= 6;
h = figure;
box on
colors= [0,0,1;... WDA
    0 1 0;...SVM S + T
    0.7 0.1 0.7;... NN S +T
    0 0 0;... SA
    1 0 0.5;... EA ++
    0 0.6  1;...GFK
    0.4 0.4 0.6;...SSL
    1 0 0 ;...JGSA
    0 0.5 0;...LDADA
    1 0.5 0; ...SCA
    1 0.4 0.6]; ...DASGA
set(gca, 'ColorOrder', colors, 'NextPlot', 'replacechildren');


ber_wda = zeros(size(errors_wda,1),1); 
ber_svm_st = zeros(size(errors_svm_st,1),1); 
ber_nn_st = zeros(size(errors_nn_st,1),1); 
ber_ea = zeros(size(errors_ea,1),1); 
ber_sa = zeros(size(errors_sa,1),1); 
ber_jgsa = zeros(size(errors_jgsa,1),1); 
ber_ldada = zeros(size(errors_ldada,1),1); 
ber_sca = zeros(size(errors_sca,1),1); 
ber_gfk = zeros(size(errors_gfk,1),1); 
ber_ssl = zeros(size(errors_ssl,1),1); 
ber_ssl_cmn = zeros(size(errors_ssl_cmn,1),1); 
ber_dasga = zeros(size(errors_dasga,1),1); 


for i = 1 : size(errors_svm_st,1)
    for j = 1 : size(errors_svm_st,2)
        ber_wda(i) = ber_wda(i)+ conf2ber(errors_wda(i,j).conf);
        ber_svm_st(i) = ber_svm_st(i)+ conf2ber(errors_svm_st(i,j).conf);
        ber_nn_st(i) = ber_nn_st(i)+ conf2ber(errors_nn_st(i,j).conf);
        ber_ea(i) = ber_ea(i)+ conf2ber(errors_ea(i,j).conf);
        ber_sa(i) = ber_sa(i)+ conf2ber(errors_sa(i,j).conf);
        ber_jgsa(i) = ber_jgsa(i)+ conf2ber(errors_jgsa(i,j).conf);
        ber_ldada(i) = ber_ldada(i)+ conf2ber(errors_ldada(i,j).conf);
        ber_sca(i) = ber_sca(i)+ conf2ber(errors_sca(i,j).conf);
        ber_gfk(i) = ber_gfk(i)+ conf2ber(errors_gfk(i,j).conf);
        ber_ssl(i) = ber_ssl(i)+ conf2ber(errors_ssl(i,j).conf);
        ber_ssl_cmn(i) = ber_ssl_cmn(i)+ conf2ber(errors_ssl_cmn(i,j).conf);
        ber_dasga(i) = ber_dasga(i)+ conf2ber(errors_dasga(i,j).conf);
    end
end
ber_wda = ber_wda ./ size(errors_wda,2);
ber_svm_st = ber_svm_st ./ size(errors_svm_st,2);
ber_nn_st = ber_nn_st ./ size(errors_nn_st,2);
ber_ea = ber_ea ./ size(errors_ea,2);
ber_sa = ber_sa ./ size(errors_sa,2);
ber_jgsa = ber_jgsa ./ size(errors_jgsa,2);
ber_ldada = ber_ldada ./ size(errors_ldada,2);
ber_sca = ber_sca ./ size(errors_sca,2);
ber_gfk = ber_gfk ./ size(errors_gfk,2);
ber_ssl = ber_ssl ./ size(errors_ssl,2);
ber_ssl_cmn = ber_ssl_cmn ./ size(errors_ssl_cmn,2);
ber_dasga = ber_dasga ./ size(errors_dasga,2);





plot(param_vect, 100- 100*ber_wda,'*-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
hold on

plot(param_vect,100- 100*ber_svm_st, 'o-',...
   'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_nn_st, 'o-',...
   'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*ber_sa, 'x-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*ber_ea, 'v-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_gfk, '<-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*ber_ssl_cmn, '>-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_jgsa,'>-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*ber_ldada,'x--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	
plot(param_vect,100- 100*ber_sca,'s--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	
plot(param_vect,100- 100*ber_dasga,'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	

ylim([25 70])
L = legend('GrALP', 'SVM',...
    'NN','SA','EA++',...
    'GFK','SSL','JGSA','LDADA','SCA','DASGA','FontSize',14,'NumColumns',1);

set(gcf,'Units','Inches');
 pos = get(gcf,'Position');
  set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
xlabel('Ratio of matched nodes (%)');
ylabel('Misclassification rate (%)');
 set(gca,'fontsize',18) 

print(gcf,'Facebook_all_labeled_match_c.pdf','-dpdf','-r0')
saveas(gcf,'Facebook_all_labeled_match_c.fig')