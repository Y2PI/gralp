clear all
% close all
load match/NodeStruct.mat

load match/errors_wda.mat
load match/errors_svm_st.mat
load match/errors_nn_st.mat
load match/errors_sa.mat
load match/errors_ea.mat
load match/errors_gfk.mat
load match/errors_jgsa.mat
load match/errors_ldada.mat

for(j=1:size(NodeStruct(1).sourceknownnodes,2))
    
    param_vect(j) = 100*length(NodeStruct(1).matchsourcenodes{j})/61;
end
% Number of known target labels
LineWidth = 1.5;
MarkerSize= 6;
h = figure;
box on
colors= [0,0,1;...WDA
    0 1 0;...SVM S
    0.7 0.1 0.7;... NN S
    0 0 0;...SA
    1 0 0.5;... EA ++
    0 0.6  1;...GFK
    1 0 0  ;...JGSA
    0 0.5 0;]; ... LDADA
    set(gca, 'ColorOrder', colors, 'NextPlot', 'replacechildren');

for i = 1 : size(errors_wda,1)
    for j = 1 : size(errors_wda,2)
        conf_wda(i,j,:,:) = errors_wda(i,j).conf;
        conf_svm_st(i,j,:,:) = errors_svm_st(i,j).conf;
        conf_nn_st(i,j,:,:) = errors_nn_st(i,j).conf;
        conf_ea(i,j,:,:) = errors_ea(i,j).conf;
        conf_sa_nn(i,j,:,:) = errors_sa(i,j).conf;
        conf_jgsa(i,j,:,:) = errors_jgsa(i,j).conf;
        conf_ldada(i,j,:,:) = errors_ldada(i,j).conf;
        conf_gfk(i,j,:,:) = errors_gfk(i,j).conf;
        
    end
end
mean_conf_wda = reshape(mean(conf_wda,2),[length(param_vect),2,2]);
mean_conf_svm_st = reshape(mean(conf_svm_st,2),[length(param_vect),2,2]);
mean_conf_nn_st = reshape(mean(conf_nn_st,2),[length(param_vect),2,2]);
mean_conf_ea = reshape(mean(conf_ea,2),[length(param_vect),2,2]);
mean_conf_sa_nn = reshape(mean(conf_sa_nn,2),[length(param_vect),2,2]);
mean_conf_jgsa = reshape(mean(conf_jgsa,2),[length(param_vect),2,2]);
mean_conf_ldada = reshape(mean(conf_ldada,2),[length(param_vect),2,2]);
mean_conf_gfk = reshape(mean(conf_gfk,2),[length(param_vect),2,2]);

% Accuracy for label 0
prec_label0_wda = mean_conf_wda(:,1,1)./(mean_conf_wda(:,1,1)+mean_conf_wda(:,1,2));
prec_label0_svm_st = mean_conf_svm_st(:,1,1)./(mean_conf_svm_st(:,1,1)+mean_conf_svm_st(:,1,2));
prec_label0_nn_st = mean_conf_nn_st(:,1,1)./(mean_conf_nn_st(:,1,1)+mean_conf_nn_st(:,1,2));
prec_label0_ea = mean_conf_ea(:,1,1)./(mean_conf_ea(:,1,1)+mean_conf_ea(:,1,2));
prec_label0_sa_nn = mean_conf_sa_nn(:,1,1)./(mean_conf_sa_nn(:,1,1)+mean_conf_sa_nn(:,1,2));
prec_label0_jgsa = mean_conf_jgsa(:,1,1)./(mean_conf_jgsa(:,1,1)+mean_conf_jgsa(:,1,2));
prec_label0_ldada = mean_conf_ldada(:,1,1)./(mean_conf_ldada(:,1,1)+mean_conf_ldada(:,1,2));
prec_label0_gfk = mean_conf_gfk(:,1,1)./(mean_conf_gfk(:,1,1)+mean_conf_gfk(:,1,2));

% Accuracy for label 1
prec_label1_wda = mean_conf_wda(:,2,2)./(mean_conf_wda(:,2,1)+mean_conf_wda(:,2,2));
prec_label1_svm_st = mean_conf_svm_st(:,2,2)./(mean_conf_svm_st(:,2,1)+mean_conf_svm_st(:,2,2));
prec_label1_nn_st = mean_conf_nn_st(:,2,2)./(mean_conf_nn_st(:,2,1)+mean_conf_nn_st(:,2,2));
prec_label1_ea = mean_conf_ea(:,2,2)./(mean_conf_ea(:,2,1)+mean_conf_ea(:,2,2));
prec_label1_sa_nn = mean_conf_sa_nn(:,2,2)./(mean_conf_sa_nn(:,2,1)+mean_conf_sa_nn(:,2,2));
prec_label1_jgsa = mean_conf_jgsa(:,2,2)./(mean_conf_jgsa(:,2,1)+mean_conf_jgsa(:,2,2));
prec_label1_ldada = mean_conf_ldada(:,2,2)./(mean_conf_ldada(:,2,1)+mean_conf_ldada(:,2,2));
prec_label1_gfk = mean_conf_gfk(:,2,2)./(mean_conf_gfk(:,2,1)+mean_conf_gfk(:,2,2));

plot(param_vect, 100- 100*(prec_label1_wda+prec_label0_wda)/2,'*-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
hold on
plot(param_vect, 100- 100* (prec_label1_svm_st+prec_label0_svm_st)/2, 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_nn_st+prec_label0_nn_st)/2, 'o-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*(prec_label1_sa_nn+prec_label0_sa_nn)/2, 'x-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*(prec_label1_ea+prec_label0_ea)/2, 'v-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_gfk+prec_label0_gfk)/2, '<-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_jgsa+prec_label0_jgsa)/2,'>-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_ldada+prec_label0_ldada)/2,'x--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);


xlabel('Ratio of matched nodes (%)','FontSize',16);
ylim([35 70])
ylabel('Misclassification rate (%)','FontSize',16);
L = legend('GrALP', 'SVM', 'NN','SA','EA++','GFK','JGSA','LDADA','FontSize',14,'NumColumns',1);

% L.Position = [0.2834    0.4131    0.6132    0.1833];

set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
set(gca,'fontsize',18)

print(gcf,'Facebook_match.pdf','-dpdf','-r0')
saveas(gcf,'Facebook_match.fig')
