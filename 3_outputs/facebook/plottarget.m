clear all

load target/NodeStruct.mat

load target/errors_wda.mat
load target/errors_svm_st.mat
load target/errors_nn_st.mat
load target/errors_sa.mat
load target/errors_ea.mat
load target/errors_gfk.mat
load target/errors_ssl.mat
load target/errors_ssl_cmn.mat
load target/errors_jgsa.mat
load target/errors_ldada.mat
load target/errors_sca.mat

 for(j=1:size(NodeStruct(1).sourceknownnodes,2))
    param_vect(j) = 100*length(NodeStruct(1).targetknownnodes{j})/61;
end

% Number of known target labels
LineWidth = 1.5;
MarkerSize= 6;
h = figure;
box on
colors= [0,0,1;... WDA
    0 1 0;...SVM S + T
    0.7 0.1 0.7;... NN S +T
    0 0 0;... SA
    1 0 0.5;... EA ++
    0 0.6  1;...GFK
    0.4 0.4 0.6;...SSL
    1 0 0 ;...JGSA
    0 0.5 0;...LDADA
    1 0.5 0];...SCA
set(gca, 'ColorOrder', colors, 'NextPlot', 'replacechildren');
for i = 1 : size(errors_svm_st,1)
    for j = 1 : size(errors_svm_st,2)
        conf_wda(i,j,:,:) = errors_wda(i,j).conf;
        conf_svm_st(i,j,:,:) = errors_svm_st(i,j).conf;
        conf_nn_st(i,j,:,:) = errors_nn_st(i,j).conf;
        conf_ea(i,j,:,:) = errors_ea(i,j).conf;
        conf_sa_nn(i,j,:,:) = errors_sa(i,j).conf;
        conf_jgsa(i,j,:,:) = errors_jgsa(i,j).conf;
        conf_ldada(i,j,:,:) = errors_ldada(i,j).conf;
        conf_sca(i,j,:,:) = errors_sca(i,j).conf;
        conf_gfk(i,j,:,:) = errors_gfk(i,j).conf;
        conf_ssl(i,j,:,:) = errors_ssl(i,j).conf;
        conf_sslcmn(i,j,:,:) = errors_ssl_cmn(i,j).conf;
    end
end
mean_conf_wda = reshape(mean(conf_wda,2),[length(param_vect),2,2]);
mean_conf_svm_st = reshape(mean(conf_svm_st,2),[length(param_vect),2,2]);
mean_conf_nn_st = reshape(mean(conf_nn_st,2),[length(param_vect),2,2]);
mean_conf_ea = reshape(mean(conf_ea,2),[length(param_vect),2,2]);
mean_conf_sa_nn = reshape(mean(conf_sa_nn,2),[length(param_vect),2,2]);
mean_conf_jgsa = reshape(mean(conf_jgsa,2),[length(param_vect),2,2]);
mean_conf_ldada = reshape(mean(conf_ldada,2),[length(param_vect),2,2]);
mean_conf_sca = reshape(mean(conf_sca,2),[length(param_vect),2,2]);
mean_conf_gfk = reshape(mean(conf_gfk,2),[length(param_vect),2,2]);
mean_conf_ssl = reshape(mean(conf_ssl,2),[length(param_vect),2,2]);
mean_conf_sslcmn = reshape(mean(conf_sslcmn,2),[length(param_vect),2,2]);
% Accuracy for label 0
prec_label0_wda = mean_conf_wda(:,1,1)./(mean_conf_wda(:,1,1)+mean_conf_wda(:,1,2));
prec_label0_svm_st = mean_conf_svm_st(:,1,1)./(mean_conf_svm_st(:,1,1)+mean_conf_svm_st(:,1,2));
prec_label0_nn_st = mean_conf_nn_st(:,1,1)./(mean_conf_nn_st(:,1,1)+mean_conf_nn_st(:,1,2));
prec_label0_ea = mean_conf_ea(:,1,1)./(mean_conf_ea(:,1,1)+mean_conf_ea(:,1,2));
prec_label0_sa_nn = mean_conf_sa_nn(:,1,1)./(mean_conf_sa_nn(:,1,1)+mean_conf_sa_nn(:,1,2));
prec_label0_jgsa = mean_conf_jgsa(:,1,1)./(mean_conf_jgsa(:,1,1)+mean_conf_jgsa(:,1,2));
prec_label0_ldada = mean_conf_ldada(:,1,1)./(mean_conf_ldada(:,1,1)+mean_conf_ldada(:,1,2));
prec_label0_sca = mean_conf_sca(:,1,1)./(mean_conf_sca(:,1,1)+mean_conf_sca(:,1,2));
prec_label0_gfk = mean_conf_gfk(:,1,1)./(mean_conf_gfk(:,1,1)+mean_conf_gfk(:,1,2));
prec_label0_ssl = mean_conf_ssl(:,1,1)./(mean_conf_ssl(:,1,1)+mean_conf_ssl(:,1,2));
prec_label0_sslcmn = mean_conf_sslcmn(:,1,1)./(mean_conf_sslcmn(:,1,1)+mean_conf_sslcmn(:,1,2));
% Accuracy for label 1
prec_label1_wda = mean_conf_wda(:,2,2)./(mean_conf_wda(:,2,1)+mean_conf_wda(:,2,2));
prec_label1_svm_st = mean_conf_svm_st(:,2,2)./(mean_conf_svm_st(:,2,1)+mean_conf_svm_st(:,2,2));
prec_label1_nn_st = mean_conf_nn_st(:,2,2)./(mean_conf_nn_st(:,2,1)+mean_conf_nn_st(:,2,2));
prec_label1_ea = mean_conf_ea(:,2,2)./(mean_conf_ea(:,2,1)+mean_conf_ea(:,2,2));
prec_label1_sa_nn = mean_conf_sa_nn(:,2,2)./(mean_conf_sa_nn(:,2,1)+mean_conf_sa_nn(:,2,2));
prec_label1_jgsa = mean_conf_jgsa(:,2,2)./(mean_conf_jgsa(:,2,1)+mean_conf_jgsa(:,2,2));
prec_label1_ldada = mean_conf_ldada(:,2,2)./(mean_conf_ldada(:,2,1)+mean_conf_ldada(:,2,2));
prec_label1_sca = mean_conf_sca(:,2,2)./(mean_conf_sca(:,2,1)+mean_conf_sca(:,2,2));
prec_label1_gfk = mean_conf_gfk(:,2,2)./(mean_conf_gfk(:,2,1)+mean_conf_gfk(:,2,2));
prec_label1_ssl = mean_conf_ssl(:,2,2)./(mean_conf_ssl(:,2,1)+mean_conf_ssl(:,2,2));
prec_label1_sslcmn = mean_conf_sslcmn(:,2,2)./(mean_conf_sslcmn(:,2,1)+mean_conf_sslcmn(:,2,2));
plot(param_vect, 100- 100*(prec_label1_wda+prec_label0_wda)/2,'*-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
hold on

plot(param_vect,100- 100* (prec_label1_svm_st+prec_label0_svm_st)/2, 'o-',...
   'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_nn_st+prec_label0_nn_st)/2, 'o-',...
   'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*(prec_label1_sa_nn+prec_label0_sa_nn)/2, 'x-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*(prec_label1_ea+prec_label0_ea)/2, 'v-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_gfk+prec_label0_gfk)/2, '<-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect, 100- 100*(prec_label1_ssl+prec_label0_ssl)/2, '>-',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_jgsa+prec_label0_jgsa)/2,'>-.',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);
plot(param_vect,100- 100*(prec_label1_ldada+prec_label0_ldada)/2,'x--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	
plot(param_vect,100- 100*(prec_label1_sca+prec_label0_sca)/2,'s--',...
    'LineWidth',LineWidth,'MarkerSize',MarkerSize);	


ylim([25 70])
L = legend('GrALP', 'SVM',...
    'NN','SA','EA++',...
    'GFK','SSL','JGSA','LDADA','SCA','FontSize',14,'NumColumns',1);

set(gcf,'Units','Inches');
 pos = get(gcf,'Position');
  set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
xlabel('Ratio of known target labels (%)');
ylabel('Misclassification rate (%)');
 set(gca,'fontsize',18) 

print(gcf,'Facebook_target.pdf','-dpdf','-r0')
saveas(gcf,'Facebook_target.fig')